const HtmlWebPackPlugin = require("html-webpack-plugin");
const webpack = require('webpack');

const htmlPlugin = new HtmlWebPackPlugin({
    template: "./src/index.html",
    filename: "./index.html"
});

module.exports = {
    output: {
        publicPath: '/',
        chunkFilename: 'assets/react/js/[chunkhash].chunk.js',
        // chunkFilename: 'assets/react/js/[name].chunk.js',
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2|jpg|mp3)$/,
                loader: "file-loader"
            },
            {
                test: /\.(jpg|png|gif|svg|pdf|ico)$/,
                use: ['url-loader']
            }
        ]
    },
    plugins: [htmlPlugin,
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
    ],
    devServer: {
        historyApiFallback: true,
        // https: true
        // open: true
    },
    optimization: {
        nodeEnv: 'production',
        removeEmptyChunks: true,
        mergeDuplicateChunks: false,
        flagIncludedChunks: true,
        splitChunks: {
            chunks: 'all',
            cacheGroups: {
                vendors: {
                    reuseExistingChunk: true
                },
                commons: {
                    name: 'commons',
                    chunks: 'initial',
                    minChunks: 2
                }
            }
        },
        runtimeChunk: {
            name: 'runtime'
        }
    }
};