import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Ionicon from 'react-ionicons';
import { isLoggedIn, loggedInUserDetails } from '../../helper/authentication';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import profile_img from '../../assets/img/acc-default.png';
import './raisedbutton.css';
import TemplateWithoutSlideBar from '../../template/template-without-slide-bar';
import { BASE_URL_PROFILE } from '../../http-api';

class EditProfile extends Component {
	setInputFile = () => {
		document.getElementById('profilePic').click()
	}
    render() {
		const userDetails = loggedInUserDetails();
        return(
			<React.Fragment>
                <div style={{background: '#eeeeee', height: '10px', marginTop: '1px'}}></div>
                    <div className="block-title">
                        <h3>Edit Profile</h3>
                    </div>
                    <div className="block pd-15">
			            <div className="list-block">
			                <ul>
				                <li>
					                <input type="text" name="name" className="form-control my-input" id="name" placeholder="Username" value={userDetails.username}/>
				                </li>
				                <li className="text-center">
									<div style={{width: '170px', height: '170px', overflow: 'hidden', margin: '25px auto 0px auto', borderRadius: '50%', position: 'relative'}}>
										{/* <img src={profile_img} width="170px" alt="" /> */}
										<LazyLoadImage
											effect="blur"
											height="140px"
											width="140px"
											src={BASE_URL_PROFILE+userDetails.image}
											style={{borderRadius: '50%'}}
										/>
									</div>
									<button className="material-button-raised" onClick={this.setInputFile}>
										<Ionicon icon="md-camera" fontSize="2em" color="#fff" style={{marginTop: '11px'}}/>
									</button>
									<input id="profilePic" type="file" name="pic" accept=".png, .jpg, .jpeg" style={{display: 'none'}} />
				                </li>
			                </ul>
			            </div>
			        <div className="clearfix"></div>
		        </div>
		        <div className="block pd-15 text-center">
			        <Link to="/profile" className="btn btn-primary btn-round">Submit</Link>
		        </div>
				</React.Fragment>
        );
    }
};

export default EditProfile;