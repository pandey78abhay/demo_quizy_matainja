import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { isLoggedIn, loggedInUserDetails } from '../../helper/authentication';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

import profile_img from '../../assets/img/acc-default.png';
import { BASE_URL_PROFILE } from '../../http-api';
class ProfileDetails extends Component {

    render() {
		// USER Details
		const userDetails = loggedInUserDetails();

        return(
            <React.Fragment>
                <div style={{background: '#eeeeee', height: '10px', marginTop: '1px'}}></div>
		        <div className="block-title">
			        <h3>Profile</h3>
			        <Link to="edit" className="button-edit">
				        <i className="fa fa-edit"></i>
			        </Link>
		        </div>
                <div className="block pd-15">
			        <div className="set">
				        <div className="set-img">
							{/* <img src={profile_img} className="img-circle" alt=""/>  */}
							<LazyLoadImage
                                effect="blur"
                                height="140px"
                                width="140px"
                                src={BASE_URL_PROFILE+userDetails.image}
                                style={{borderRadius: '50%'}}
                            />
				        </div>
				        <div className="set-bar">
					        <div className="set-extra">
						        <h5 className="desc1">Username: 
							        <span>{userDetails.username}</span>
						        </h5>
					        </div>
					        <div className="set-extra">
						        <h5 className="desc2">Msisdn: 
							        <span>{userDetails.msisdn}</span>
						        </h5>
					        </div>
					        <div className="set-extra">
						        <h5 className="desc1">Status: 
							        <span>Subscriber</span>
						        </h5>
					        </div>
				        </div>
				        <div className="clearfix"></div>
			        </div>
			        <div className="col-xs-6 mt-20">
				        <a href="" className="btn btn-primary btn-round bg-1">Unsubscribe</a>
			        </div>
			        <div className="col-xs-6 mt-20">
				        <a href="" className="btn btn-primary btn-round">Renewal</a>
			        </div>
			        <div className="clearfix"></div>
		        </div>
            </React.Fragment>
        );
    }
};

export default ProfileDetails;