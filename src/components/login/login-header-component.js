'use strict';
import React, {Component} from 'react';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
class LoginHeaderComponent extends Component {
    responseFacebook = (response) => {
        console.table(response);
        const url = response.picture.data.url;
        console.log(url);
        var xmlHTTP = new XMLHttpRequest();
        xmlHTTP.open('GET', url, true);
        xmlHTTP.responseType = 'arraybuffer';
        xmlHTTP.onload = function (e) {
            var arr = new Uint8Array(this.response);
            var raw = String.fromCharCode.apply(null, arr);
            var b64 = btoa(raw);
            var dataURL = "data:image/png;base64," + b64;
            console.log(dataURL);
        };
        xmlHTTP.send();
    }
    responseGoogle = (response) => {
        console.log(response);
        const url = response.profileObj.imageUrl;
        console.log(url);
        var xmlHTTP = new XMLHttpRequest();
        xmlHTTP.open('GET', url, true);
        xmlHTTP.responseType = 'arraybuffer';
        xmlHTTP.onload = function (e) {
            var arr = new Uint8Array(this.response);
            var raw = String.fromCharCode.apply(null, arr);
            var b64 = btoa(raw);
            var dataURL = "data:image/png;base64," + b64;
            console.log(dataURL);
        };
        xmlHTTP.send();
    }
    render() {
        return(
            <div className="header header-warning text-center">
                <h4 className="card-title">LOG IN</h4>
                {/* <div className="social-line">
                    <a href="#facebook" className="btn btn-just-icon btn-simple">
                        <i className="fab fa-2x fa-facebook-square"></i>
                    </a> */}
                    {/* <FacebookLogin
                        appId="761413570891093"
                        fields="name,email,picture"
                        callback={this.responseFacebook}
                        cssClass="my-facebook-button-class"
                        textButton=''
                        icon="fa-facebook-square"
                    /> */}
                    {/* <a href="#twitter" className="btn btn-just-icon btn-simple">
                        <i className="fab fa-2x fa-twitter-square"></i>
                    </a>
                    <a href="#google" className="btn btn-just-icon btn-simple">
                        <i className="fab fa-2x fa-google-plus-square"></i>
                    </a> */}
                    {/* <GoogleLogin
                        clientId="288699278623-f6bgc1n855hs5bcl99qpindbpvr16m0g.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={this.responseGoogle}
                        onFailure={this.responseGoogle}
                    /> */}
                {/* </div> */}
            </div>
        );
    }
}
export default LoginHeaderComponent;