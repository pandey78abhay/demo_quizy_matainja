import React, { Component } from 'react';
import Ionicon from 'react-ionicons';
import { post } from '../../http-api';
import Swal from 'sweetalert2';
import { withRouter } from 'react-router-dom';

const styles = {
    position: 'relative',
    top: '-12px'
};


const LoginLoadingButton = (props) => (
    <div type="button" className="btn btn-primary btn-round" style={{padding: '0px 50px', height: '42px', overflow: 'hidden'}}>
        <svg class="lds-message" width="50px" height="50px" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style={{background: 'rgba(0, 0, 0, 0) none repeat scroll 0% 0%', marginTop: '-2px'}}>
            <g transform="translate(20 50)">
                <circle cx="0" cy="0" r="8" fill="#ffffff">
                    <animateTransform attributeName="transform" type="scale" begin="-0.375s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"/>
                </circle>
            </g>
            <g transform="translate(40 50)">
                <circle cx="0" cy="0" r="8" fill="#ffffff">
                    <animateTransform attributeName="transform" type="scale" begin="-0.25s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"/>
                </circle>
            </g>
            <g transform="translate(60 50)">
                <circle cx="0" cy="0" r="8" fill="#ffffff">
                    <animateTransform attributeName="transform" type="scale" begin="-0.125s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"/>
                </circle>
            </g>
            <g transform="translate(80 50)">
                <circle cx="0" cy="0" r="8" fill="#ffffff">
                    <animateTransform attributeName="transform" type="scale" begin="0s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"/>
                </circle>
            </g>
        </svg>
    </div>
);



class LoginBodyComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            msisdn: '',
            password: '',
            logining: false,
            error: {},
            showPassword: false
        };
    }
    togglePassword = () => {
        this.setState((prevState)=>({showPassword: !prevState.showPassword}));
    }
    onChangeHundler = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }
    onClickLogin = () => {
        const { msisdn, password } = this.state;
        const error = {};
        if (msisdn.trim() === '') {
            error.msisdn = true;
        }
        if (password.trim() === '') {
            error.password = true
        }
        if(!Object.keys(error).length) {
            this.setState({logining: true});
            const payload = new FormData();
            payload.append('msisdn', msisdn);
            payload.append('password', password);
            post('login', payload) .then(res => {
                if(res.data.success){
                    localStorage.setItem('JWT', res.data.data.JWT);
                    localStorage.setItem('user_details', JSON.stringify(res.data.data.user_details));
                    localStorage.setItem('userDetailsSplash', JSON.stringify(res.data.data.user_details));
                    this.setState({ logining: false, msisdn: '', password: ''});
                    this.props.history.goBack();
                } else {
                    Swal(res.data.message,'','error');
                    this.setState({logining: false});
                }
            }).catch (function (error) {
                if (error.message === "Network Error") {
                    Swal(error.message, '', 'error').then((result)=>{
                        window.location.reload();
                    });
                }
            });
        }
        this.setState({error:error});
    }
    render() {
        const { msisdn, password, error, logining, showPassword } = this.state;
        console.log({showPassword});
        return(
            <React.Fragment>
                {/* <p className="description text-center">Or Be Classical</p> */}
                <form>
                    <div className="content">
                        <div className="input-group">
                            <span className="input-group-addon">
                                <Ionicon icon="md-call" fontSize="24px" color="#555" style={styles}/>
                            </span>
                            <div className="form-group is-empty">
                                <input type="text" className="form-control" placeholder="Enter MSISDN" name="msisdn" onChange={this.onChangeHundler} value={msisdn}
                                    style={
                                        error.msisdn?
                                        {backgroundImage: 'linear-gradient(#ff0000, #ff0000), linear-gradient(#ff0000, #ff0000)'}:
                                        {backgroundImage: 'linear-gradient(#9c27b0, #9c27b0), linear-gradient(#D2D2D2, #D2D2D2)'}
                                    }
                                />
                                <span className="material-input"></span>
                            </div>
                        </div>
                        <div className="input-group">
                            <span className="input-group-addon">
                                <Ionicon icon="md-lock" fontSize="24px" color="#555" style={styles}/>
                            </span>
                            <div className="form-group is-empty" style={{position: 'relative'}}>
                                <input type={showPassword? 'text':'password'} placeholder="Enter Password" className="form-control" name="password" onChange={this.onChangeHundler} value={password}
                                    style={
                                        error.password?
                                        {backgroundImage: 'linear-gradient(#ff0000, #ff0000), linear-gradient(#ff0000, #ff0000)'}:
                                        {backgroundImage: 'linear-gradient(#9c27b0, #9c27b0), linear-gradient(#D2D2D2, #D2D2D2)'}
                                    }
                                />
                                <div className="password-show-hide" style={{
                                    position: 'absolute',
                                    right: 0,
                                    zIndex: 999,
                                    top: '6px',
                                    bottom: 0
                                }}>
                                    {!showPassword?
                                        <Ionicon onClick={()=>this.togglePassword()} icon="md-eye" fontSize="20px" color="#555" />:
                                        <Ionicon onClick={()=>this.togglePassword()} icon="md-eye-off" fontSize="20px" color="#555" />}
                                </div>
                                <span className="material-input"></span>
                            </div>
                        </div>
                    </div>
                    <div className="footer text-center mb-10">
                        {logining? <LoginLoadingButton />:
                        <button 
                            type="button"
                            onClick={this.onClickLogin}
							class="pure-material-button-contained"
							style={{height: '42px', width: '150px', textTransform: 'capitalize'}}
						>Get Started</button>}
                    </div>
                </form>
            </React.Fragment>
        );
    }
};

export default withRouter(LoginBodyComponent);
