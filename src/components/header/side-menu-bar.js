import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import classNames from 'classnames';
import { isLoggedIn, loggedInUserDetails } from '../../helper/authentication';
import profile_img from '../../assets/img/acc-default.png';
import gold_coin from '../../assets/gold_coin.png';
import {selectedLanguage} from '../../helper/authentication';
class LeftSideMenuBar extends Component {
        constructor(props) {
            super(props);
            this.state = {
                logout: false
            }
        }

        componentDidMount() {
            document.addEventListener('mousedown', this.handleClickOutside);
        }

        componentWillUnmount() {
            document.removeEventListener('mousedown', this.handleClickOutside);
        }

        setWrapperRef = (node) => {
            this.wrapperRef = node;
        }

        handleClickOutside = (event) => {
            if (event.target.className !== "button-collapse navbar-toggle nav-toggle" &&
                this.wrapperRef && !this.wrapperRef.contains(event.target)) {
                this.props.closeSideMenuBar();
            }
        }
        logout = () => {
            localStorage.clear();
            // temporary It must have to be changed
            window.location.href="/";
        }
        render() {
        const { open, closeSideMenuBar, logout, location } = this.props;
        const language = { "en": "English", "id": "Indonesia", "ms": "Malaysia", "nl": "Deutch" }; 
        return(
            <nav className={classNames("sideNav", "bgImg2", "toporange", {"open": open})}>
                <div className="page-content notop">
	                <div className="account">
		                <AccountComponent closeSideMenuBar={closeSideMenuBar}/>
	                </div>
	                <div ref={this.setWrapperRef} className="list-block mt-15">
                        <div className="list-group">
                            <nav>
				                <div className="list-block">
                                    <ul>
                                        <li className="divider">Menu</li>
					                    <li>
                                            {
                                                isLoggedIn() ?
                                                <a className="item-link close-panel item-content" onClick={()=>{this.logout(); closeSideMenuBar()}}>
                                                    <div className="item-media">
                                                        <i className="fa fa-sign-out-alt"></i>
                                                    </div>
                                                    <div className="item-inner">
                                                        <div className="item-title">Logout</div>
                                                    </div>
                                                </a> : 
                                                <CustomSideBarLink path="/login" closeSideMenuBar={closeSideMenuBar}>
                                                    <div className="item-media">
                                                        <i className="fa fa-sign-in-alt"></i>
                                                    </div>
                                                    <div className="item-inner">
                                                        <div className="item-title">Login</div>
                                                    </div>
                                                </CustomSideBarLink>
                                            }
                                        </li>
                                        <li>
                                            <CustomSideBarLink path={"/quiz/category"} closeSideMenuBar={closeSideMenuBar}>
                                                <div className="item-media">
                                                    <i className="fa fa-bookmark"></i>
                                                </div>
                                                <div className="item-inner">
                                                    <div className="item-title">Quiz Category</div>                                
                                                </div>
                                            </CustomSideBarLink>
                                        </li> 
					                    <li>
                                            <CustomSideBarLink path={"/rewards"} closeSideMenuBar={closeSideMenuBar}> 
                                                <div className="item-media">
                                                    <i className="fa fa-trophy"></i>
                                                </div> 
                                                <div className="item-inner">
                                                    <div className="item-title">Rewards</div>
                                                </div>
                                            </CustomSideBarLink>
                                        </li>
                                        <li>
                                            <CustomSideBarLink path={"/leaderboard/all"} closeSideMenuBar={closeSideMenuBar}>
                                                <div className="item-media">
                                                    <i className="fa fa-trophy"></i> 
                                                </div>
                                                <div className="item-inner">
                                                    <div className="item-title">Leader Board</div>
                                                </div>
                                            </CustomSideBarLink>
                                        </li>
                                        <li> 
                                            <CustomSideBarLink path={{pathname:"/lang", state: { from: location.pathname} }} closeSideMenuBar={closeSideMenuBar}>
							                    <div className="item-media">
								                    <i className="fa fa-globe"></i>
							                    </div>
							                    <div className="item-inner">
								                    <div className="item-title">Language</div>
								                    <div className="item-after">{language[selectedLanguage()]}</div>
							                    </div>
						                    </CustomSideBarLink>
                                        </li>
                                        <li className="divider"></li>
					                    <li>
                                            <CustomSideBarLink path={"/frequently-asked-questions"} closeSideMenuBar={closeSideMenuBar}>
                                                <div className="item-media">
                                                    <i className="fa fa-question-circle"></i>
                                                </div>
                                                <div className="item-inner">
                                                    <div className="item-title">FAQs</div>
                                                </div>
                                        </CustomSideBarLink>
                                        </li>
                                        <li>
                                            <CustomSideBarLink path={"/privacy-policy"} closeSideMenuBar={closeSideMenuBar}>
                                                <div className="item-media">
                                                    <i className="fa fa-question-circle"></i>
                                                </div>
                                                <div className="item-inner">
                                                    <div className="item-title">Privacy Policy</div>
                                                </div>
                                            </CustomSideBarLink>
                                        </li>
                                        <li>
                                            <CustomSideBarLink path={"/terms-of-service"} closeSideMenuBar={closeSideMenuBar}>
                                                <div className="item-media">
                                                    <i className="fa fa-question-circle"></i>
                                                </div>
                                                <div className="item-inner">
                                                    <div className="item-title">Terms of Service</div>
                                                </div>
                                            </CustomSideBarLink>
                                        </li>
                                        <li className="divider"></li>
                                        <li style={{paddingLeft: '0px'}}>
                                            <div className="follow">
                                                <span className="footer-text">Follow us</span>
                                                <div className="social-media-icons">
                                                    <a href="" target="_blank" className="medium-fb">
                                                        <span className="social-icon icon-fb"><i className="fab fa-facebook-square"></i></span>
                                                    </a>
                                                    <a href="" target="_blank" className="medium-twitter">
                                                        <span className="social-icon icon-twitter"><i className="fab fa-twitter"></i></span>
                                                    </a>
                                                    <a href="" target="_blank" className="medium-youtube">
                                                        <span className="social-icon icon-youtube"><i className="fab fa-youtube"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul> 
				                </div> 
                            </nav>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
};

// Functional Component For Creating Custom Link for Side Menu Bar
const CustomSideBarLink = (props) => {
    return(
        <Link to={props.path} className="item-link close-panel item-content" onClick={props.closeSideMenuBar}>
            {props.children}
        </Link>
    );
}

// Functional Component For Creating Account Component
const AccountComponent = (props) => {
    const userDetails = loggedInUserDetails();
    const base_url = `http://demo.quizy.mobi/assets/uploads/profiles/`;
    return(
        <div className="list-block media-list">
        <ul>
            <li>
                {
                    userDetails?
                        <Link to="/profile" className="item-link item-content d-flx" onClick={props.closeSideMenuBar}>
                            <div className="item-media">
                                <img src={`${base_url}${userDetails.image}`} width="80" style={{borderRadius: '50%'}}/>
                            </div> 
                            <div className="item-inner">
                                <div className="item-title text-white" style={{top: '8px'}}>{userDetails.first_name} {userDetails.last_name}</div>
                                <div style={{position: 'absolute', right: '10px', top: '-2px', zIndex: 1000}}>
                                    <img src={gold_coin} style={{height: '40px', position: 'relative', zIndex: 1}}/>
                                    <span class="badge" style={{
                                            fontSize: '14px', 
                                            marginLeft: '-8px',
                                            padding: '5px 7px 5px 8px',
                                            borderRadius:'5px', 
                                            backgroundColor: '#FF4E1A'
                                    }}>{userDetails.coin}</span>
                                </div>
                            </div>
                        </Link>
                    :
                        <a className="item-link item-content d-flx" onClick={props.closeSideMenuBar}>
                            <div className="item-media">
                                <img src={profile_img} width="80" style={{borderRadius: '50%'}}/>
                            </div> 
                            <div className="item-inner">
                                <div className="item-title text-white">Demo Azik</div> 	
                            </div>
                        </a>
                }
            </li> 
        </ul>
    </div>
    );
};


export default withRouter(LeftSideMenuBar);