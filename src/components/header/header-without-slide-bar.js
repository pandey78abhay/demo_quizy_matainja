import React from "react";
import { Link, withRouter } from 'react-router-dom';
import Ionicon from 'react-ionicons';

import logo from '../../assets/img/logo.png';

const HeaderWithoutSlidebar = (props) => {
    console.log(props);
    const { location } = props;

    return(<React.Fragment>
        <div id="toolbar" className="bluedark toporange botorange">
            <div className="open-left">
                <a onClick={()=>props.history.goBack()} className="link back">
                    <Ionicon icon="md-arrow-back" fontSize="1em" color="#fff" />
                </a>
            </div>
            <div className="logo m-0">
                <Link to='/'>
                    <img src={logo} alt="" style={{height: '42px'}}/>
                </Link>
            </div>
            <div className="open-right" data-activates="slide-out-right">
                <Link to="/quiz/search" seach="true" className="ion-head">
                    <Ionicon icon="md-search" fontSize="1em" color="#fff" />
                </Link>
            </div>
        </div>
        <div className="clearfix"></div>
    </React.Fragment>);
};
export default withRouter(HeaderWithoutSlidebar);