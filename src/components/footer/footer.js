import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => (
	<footer>
		<div className="follow">
			<span className="footer-text">Follow us</span>
			<div className="social-media-icons">
				<a href="" target="_blank" className="medium-fb">
					<span className="social-icon icon-fb"><i className="fab fa-facebook-square"></i></span>
				</a>
				<a href="" target="_blank" className="medium-twitter">
					<span className="social-icon icon-twitter"><i className="fab fa-twitter"></i></span>
				</a>
				<a href="" target="_blank" className="medium-youtube">
					<span className="social-icon icon-youtube"><i className="fab fa-youtube"></i></span>
				</a>
			</div>
		</div>
		<div className="folmid">
			<Link to="/frequently-asked-questions">FAQ</Link> | <Link to="/privacy-policy">Privacy Policy</Link> | <Link to="/terms-of-service">Term of Service</Link>
		</div>
		<br />
		<div className="text-center text-small text-grey mt-5">@ 2018 Quizy</div>
	</footer>
);

export default Footer;