import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchTopRewards } from '../../store/actions';
import RewardsSlider from './rewards-slider';
import { RewardLoader } from '../../content-loader';
import { post } from '../../http-api';
class TopRewards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }
    componentDidMount() {
        post('gettoprewards')
            .then(res=>{
                if(res.data.success) {
                    const { reward_details } = res.data;
                    this.props.fetchTopRewards(reward_details);
                    this.setState({loading: false});
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({loading: false});
                throw new Error('Could not fetch products. Try again later.');
            });
    }
    render() {
        console.log(this.props.allRewards);
        const { topRewards } = this.props;
        const { loading } = this.state;
        if(loading)
            return(<RewardLoader />);
        return(<Fragment>{topRewards.length?<RewardsSlider rewards={topRewards} />:<RewardLoader />}</Fragment>); 
    }
};

const mapStateToProps = (state) => ({
    topRewards: state.rewards.top,
});

const mapActionsToProps = {
    fetchTopRewards: fetchTopRewards
};

export default connect(mapStateToProps, mapActionsToProps)(TopRewards);

