import React from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import Swiper from 'react-id-swiper';
import 'swiper/dist/css/swiper.min.css';
import { QuizBannerLoader } from '../../content-loader';
import {BASE_URL_IMAGE} from '../../http-api';
const SwiperSlider = (props) => {
    const params = {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false
        }
    };
    const { banner } = props;
    const slides = banner.map((banner, i)=>(
        <div key={i} style={{position: 'relative'}}>
            <Link to={`/quiz/details/${banner.id}`}>
                <img src={BASE_URL_IMAGE+banner.banner_image} alt="" />
            </Link>
            <div style={{position: 'absolute', zIndex: '1000', left: 0, bottom: 0, right: 0, width: '100%', backgroundColor: 'rgba(0,0,0,0.5)', textAlign: 'center', height: 'auto', padding: '10px', color: '#fff', pointerEvents: 'none'}}>
            {banner.banner_title}</div>
        </div>
    ));
    if (banner.length)
        return(<Swiper {...params}>{slides}</Swiper>);
    return(<QuizBannerLoader />);
};

const mapStateToProps = (state) =>({
    banner: state.banner,
});

export default connect(mapStateToProps)(SwiperSlider);
