
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import pvpImg from '../../../assets/img/pvp.png';

class categorypvp extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    categoryPVPpage = () => {
        console.log('cat')
        this.props.history.push({
            pathname: '/category-pvp'
        });
    }
    render() {
        return (

            <div className="container" onClick={this.categoryPVPpage}>
                <button type="button" class="btn"
                    style={{ backgroundColor: '#32CD32', color: '#fff', borderRadius: '50px 50px 50px 50px' }}>
                    Play Now</button>
                <img style={{ width: '50px', height: '40px', paddingLeft: '10px' }}
                    src={pvpImg} />
            </div>
        )
    }
}
export default withRouter(categorypvp);