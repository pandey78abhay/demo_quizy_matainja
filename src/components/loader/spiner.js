import React from 'react';

const Loader = () => (
    <div id="loader">
        <div id="spin"></div>
    </div>
);

export default Loader;