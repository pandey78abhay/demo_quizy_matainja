import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';

import { connect } from "react-redux";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';


import { fetchQuestionByCatId } from '../../store/actions/productActions';
import { resetPlayedQuiz, selectQuestion } from '../../store/actions';
import Loader from '../loader/spiner';
import { isLoggedIn } from '../../helper/authentication';
import Swal from 'sweetalert2';

class Details extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quizDetails: [],
        }
    }
    componentDidMount() {
        //window.scrollTo(0, 0);

        // this.loadQuizDetails(this.state.quizId);
        // console.log(this.state.quizId);
        // this.props.fetchQuestionByCatId(this.state.quizId);
    }
    componentWillReceiveProps(nextProps) {
        //console.log('nextProps');
        //window.scrollTo(0, 0);
        // this.loadQuizDetails(nextProps.quizid);
    }
    playQuiz = (quiz) => {
        console.log(quiz);
        if(isLoggedIn()){
            // Authenticated user
            this.props.resetPlayedQuiz();
            this.props.selectQuestion(1);
            Swal({
                title: quiz.title,
                html: `<h3 style="text-align: left;margin-left: 27px;margin-top: 0px;margin-bottom: 0px;">Instructions:</h3>
                        <ul>
                            <li style="text-align: left; font-size: 15px; list-style-type: none; line-height: 25px;">●&nbsp;No. of Questions: ${quiz.total_question}</li>
                            <li style="text-align: left; font-size: 15px; list-style-type: none; line-height: 25px;">●&nbsp;DO NOT Refresh the page!</li>
                            <li style="text-align: left; font-size: 15px; list-style-type: none; line-height: 25px;">●&nbsp;All The Best</li>
                        </ul>`,
                //  background: `#fff url(http://demo.quizy.mobi/assets/uploads/logo/${quiz.title_image}) no-repeat center center`,
                //  backgroundSize: 'contain',
                imageUrl: `http://demo.quizy.mobi/assets/uploads/logo/${quiz.title_image}`,
                // imageWidth: 150,
                // imageHeight: 150,
                confirmButtonText: "START",
                imageClass: 'img-responsive rounded-circle',
            }).then((result) => {
                if (result.value) {
                    this.props.fetchQuestionByCatId(quiz.id);
                    this.props.playQuiz();
                }
            });
            
        } else {
            // Not Authenticated user
            this.props.history.push({
                pathname: '/login',
                state: {from: '/quiz/details', quiz}
            });
        }
        
        
    }
    render() {
        const { location } = this.props;
        if (location.state === undefined || location.state.quiz === undefined)
            return(<Redirect to="/"/>)
        
        const { quizDetails } = this.state; 
        const base_url = `http://demo.quizy.mobi/assets/uploads/logo/`;
        return(
            <React.Fragment>
                <div style={{marginTop: '1px', textAlign: 'center'}}>
                    <LazyLoadImage
                        effect="blur"
                        height="200px"
                        src={base_url+location.state.quiz.title_image}
                    />
		        </div>
                <div className="block">
			        <div className="col-xs-8">
				        <div className="title text-title">
					        <a href="detail.html" className="text-uppercase text-dark">
						        <strong>{location.state.quiz.title}</strong>
                            </a>
				        </div>
				        <p>{location.state.quiz.total_question} Questions</p>
			        </div>
                    <div className="col-xs-4 pd-0">
                        <button
                            onClick={()=>this.playQuiz(location.state.quiz)}
							class="pure-material-button-contained"
							style={{height: '45px', width: '107px', textTransform: 'capitalize'}}
						>Play</button>
                    </div>
			        <div className="liner"></div>
                    <div className="col-xs-12">
                        <div className="text-desc" dangerouslySetInnerHTML={{ __html: location.state.quiz.description }}/>
                        <div className="text-desc" dangerouslySetInnerHTML={{ __html: location.state.quiz.description }}/>
                        <div className="text-desc" dangerouslySetInnerHTML={{ __html: location.state.quiz.description }}/>
                        <div className="text-desc" dangerouslySetInnerHTML={{ __html: location.state.quiz.description }}/>
                        <div className="text-desc" dangerouslySetInnerHTML={{ __html: location.state.quiz.description }}/>
                        <div className="text-desc" dangerouslySetInnerHTML={{ __html: location.state.quiz.description }}/>
                        <div className="text-desc" dangerouslySetInnerHTML={{ __html: location.state.quiz.description }}/>
                        <div className="text-desc" dangerouslySetInnerHTML={{ __html: location.state.quiz.description }}/>
                    </div>
		  	        {/* <div className="liner"></div> */}
                    <div className="col-xs-12">
                        <div className="">
                            <span className="footer-text text-black">Share this:</span>
                            <div className="social-media-icons">
                            <a href="" target="_blank" className="medium-fb">
                                <span className="social-icon text-black">
                                    <i className="fab fa-facebook-square"></i>
                                </span>
                            </a>
                            <a href="" target="_blank" className="medium-twitter">
                                <span className="social-icon text-black">
                                    <i className="fab fa-twitter"></i>
                                </span>
                            </a>
                        </div>
                        </div>
                    </div>
			        <div className="clearfix"></div>
		        </div>
            </React.Fragment>
        );
    }
};

const mapStateToProps = (state) =>({});

const mapActionsToProps = {
    fetchQuestionByCatId: fetchQuestionByCatId,
    resetPlayedQuiz: resetPlayedQuiz,
    selectQuestion: selectQuestion
};

export default connect(mapStateToProps,mapActionsToProps)(withRouter(Details));

