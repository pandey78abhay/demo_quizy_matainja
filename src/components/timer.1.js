import React, { Component } from 'react';
import SvgProgressBar from './svg-progress-bar';
import tick from '../assets/tick.mp3';
class Timer extends Component {
    timer=null;
    // audio = new Audio('http://demo.quizy.mobi/src/assets/tick.mp3');
    audio = new Audio(tick);
    constructor(props) {
        super(props);
        this.state = {
            time: this.props.start
        }
    }
    componentWillReceiveProps(newProps) {
        clearInterval(this.timer);
        if (newProps.stopTimer) {
            this.pauseAudio();
        } else {
            this.setState({time: newProps.start});
            this.cownDown();
        }
    }
    componentDidMount() {
        this.cownDown();
    }
    componentWillUnmount() {
        this.audio = null;
        clearInterval(this.timer);
    }
    cownDown = () => {
        this.timer=setInterval(()=>this.setState((prevState)=>{
            
            if(prevState.time===59){
                // TIME OVER
                this.pauseAudio();
                clearInterval(this.timer);
                this.props.onTimeUp('');
            } else { 
                if (document.hidden) {
                    this.pauseAudio();
                    console.log('hidden');
                }else {
                    this.playAudio();
                }
            }
            return({time: prevState.time + 1})
        }), 1000);
    }
    playAudio = () => {
        if (this.audio)
            this.audio.play();
    }
    pauseAudio = () => {
        if (this.audio)
            this.audio.pause();
    }
    render() {
        const { time } = this.state;
        console.log(this.props);
        return(
            <React.Fragment>
            <div class="circle-gr" style={{alignSelf: 'flex-end', marginLeft: 'auto', marginTop: '-15px', display: 'none'}}>
                <strong id="Progress1" class="time-number">{time<=9?'0'+time:time}</strong>
            </div>
            <SvgProgressBar cowndown={60 - time} />
            </React.Fragment>
        );
    }
};

export default Timer;