import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import Swiper from 'react-id-swiper';
import 'swiper/dist/css/swiper.min.css';
import { PopularQuizLoader } from '../../content-loader';
import { fetchPopularQuiz } from '../../store/actions/productActions';
import { post } from '../../http-api';

const params = {
    slidesPerView: 2,
    slidesPerColumn: 2,
    spaceBetween: 30,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    }
};

class PopularQuiz extends Component {
    constructor(props) {
        super(props);
        this.state = {loading: true} 
    }

    componentDidMount() {
        post('popularquiz')
        .then(res=>{
            if(res.data.success) {
                const {popularquiz} = res.data;
                this.props.fetchPopularQuiz(popularquiz);
                this.setState({loading: false});
            }
        })
        .catch(err => {
            console.log(err);
            this.setState({loading: false});
            throw new Error('Could not fetch products. Try again later.');
        });
    }

    render() {  
        const { popularQuiz } = this.props;
        console.log({popularQuiz});
        const base_url = `http://demo.quizy.mobi/assets/uploads/logo/`;
        const popular_quizs = popularQuiz.map((data, key) => (
            <div key={key} className="thumb">
                <Link to={{pathname: '/quiz/details', state: {quiz: data}}} 
                      style={{position: 'relative', display: 'block'}}
                >
                    <img
                        class="img-responsive"
                        src={base_url+data.title_image}
                        style={{height: '150px', borderRadius: '10px 10px 0px 0px', objectFit: 'cover', objectPosition: 'center'}}
                    />
                    <div className="title" style={{whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', maxWidth: '100%'}}>{data.title}</div>
                    <div className="desc" 
                    style={{position: 'absolute', top: 0, backgroundColor: 'rgba(0, 0, 0, 0.72)', width: '100%', color: 'white', borderRadius: '10px 10px 0px 0px'}}>{data.total} Questions</div>
                </Link>
            </div>
        ));
        if (popularQuiz.length)
            return(
                <React.Fragment>
                    <div className="block pd-5 m-5" style={{margin: '5px'}}>
                        <Swiper {...params}>
                            {popular_quizs}
                        </Swiper>
                        <div className="clearfix"></div>
                    </div>
                </React.Fragment>
            );
        return(<PopularQuizLoader />);
    }
};

const mapStateToProps = (state) =>({
    popularQuiz: state.quiz.popular,
});

const mapActionsToProps = {
    fetchPopularQuiz: fetchPopularQuiz,
};

export default connect(mapStateToProps, mapActionsToProps)(PopularQuiz);