import React, { Component } from 'react';
import classNames from 'classnames';
import { popular_quiz } from '../../images';
const quiz_histories = [
    {
        img: popular_quiz[2],
        title: "Social Media",
        percentOfCompletion: "77%"
    },
    {
        img: popular_quiz[1],
        title: "Gaming World Quiz",
        percentOfCompletion: "64%"
    },
    {
        img: popular_quiz[3],
        title: "World Place to Visit",
        percentOfCompletion: "53%"
    },
    {
        img: popular_quiz[0],
        title: "Bitcoin Quiz",
        percentOfCompletion: "9%"
    }
];

class QuizHistory extends Component {
    render() {
        const classes = ["hstry1", "hstry2"];
        const histories = quiz_histories.map((quiz, key)=>(
            <div key={key} className={classNames(classes[key % 2])}>
                <div className="col-xs-3">
                    <img src={quiz.img} height="40px" alt=""/> 
                </div>
                <div className="col-xs-6 pd-0">
                    <div className="text-hstry">{quiz.title}</div>
                </div>
                <div className="col-xs-3">
                    <div className="text-hstry pull-right">{quiz.percentOfCompletion}</div>
                </div>
            </div>
        ));
        return(
            <React.Fragment>
                <div className="block-title">
			        <h3>Quiz History</h3>
		        </div>
                <div className="block pd-15">
                    {histories}
			        <div className="clearfix"></div>
		        </div>
            </React.Fragment>
        );
    }
};

export default QuizHistory;