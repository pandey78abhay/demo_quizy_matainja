import React, { Component } from 'react';
import { connect } from "react-redux";
import LatestQuizSlider from './latest-quiz-slider';
import { LatestQuizLoader } from '../../content-loader';
import { post } from '../../http-api';
import { fetchLatestQuiz } from '../../store/actions';

class LatestQuiz extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        } 
    }
    componentDidMount() {
        post('latestquiz')
            .then(res=>{
                if(res.data.success) {
                    const {latestquiz} = res.data;
                    this.props.fetchLatestQuiz(latestquiz);
                    this.setState({loading: false});
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({loading: false});
                throw new Error('Could not fetch products. Try again later.');
            });
    }
    render() {
        const { latestQuiz } = this.props;
        if (latestQuiz.length)
            return(
                <React.Fragment>
                    <div className="block" style={{ padding: "15px" }}>
                        <LatestQuizSlider latestQuiz={latestQuiz}/>
                    </div>
                </React.Fragment>
            );
        return(<LatestQuizLoader />);
    }
};

const mapStateToProps = (state) =>({
    latestQuiz: state.quiz.latest,
});
const mapActionsToProps = {
    fetchLatestQuiz: fetchLatestQuiz,
};
export default connect(mapStateToProps, mapActionsToProps)(LatestQuiz);