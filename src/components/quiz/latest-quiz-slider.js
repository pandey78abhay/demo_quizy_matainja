import React from 'react';
import Swiper from 'react-id-swiper';
import { Link } from 'react-router-dom';

import 'swiper/dist/css/swiper.min.css';
import quizy_img from '../../assets/img/quizy.png';
import {BASE_URL_IMAGE} from '../../http-api';

const LatestQuizSlider = (props) => {
    const params = {
        slidesPerView: 3,
        spaceBetween: 10,
    };
    let { latestQuiz } = props;
    
    if(latestQuiz===undefined){
        latestQuiz = [];
    }

    const slides = latestQuiz.map((quiz, i) => (
        <div key={i}>
            <Link to={`/quiz/details/${quiz.id}`} className="link display-block">
                <div className="thumb">
                    <div className="cover-bg" 
                        style={quiz.title_image.length?{backgroundImage: `url(${BASE_URL_IMAGE}${quiz.title_image})`}:{backgroundImage: `url(${quizy_img})`}}
                    ></div>
                    <div className="thumb-meta">
                        <h4 style={{whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', maxWidth: '100%'}}>{quiz.title}</h4>
                        <p>{quiz.total} Questions</p>
                    </div>
                </div>
            </Link>
        </div>
    ));
    
    return(
        <Swiper {...params}>
            { slides }
        </Swiper>
    );
};

export default LatestQuizSlider;