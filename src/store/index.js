import { createStore, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import { allReducers } from "./reducers";

const allReducersEnhence = compose(
    applyMiddleware(thunk),
    // window.devToolsExtension && window.devToolsExtension()
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export const store = createStore(
    allReducers,
    allReducersEnhence
);