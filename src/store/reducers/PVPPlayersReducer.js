import {FETCH_PVP_PLAYERS} from '../types';

initialState={};

export const PVPPlayersReducer = (state=initialState,action) =>{
    switch(action.type){
        case FETCH_PVP_PLAYERS :
            return action.payload
        default:
            return state;
    }
}