import { FETCH_PVP_ANSWER_LIST } from "../types";

const initialState = [];

export const answerPVPReducecer  = (state = initialState, action) => {
    switch(action.type){
        case FETCH_PVP_ANSWER_LIST:
            return action.payload;
        default:
            return state;
    }
};