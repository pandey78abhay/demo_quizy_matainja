import React, { Component } from 'react';
import { connect } from "react-redux";
import { post } from '../http-api';
import TemplateWithGoBackButton from '../template/template-with-goback-button';
import './faq.css';
import { fetchFaqDetails } from '../store/actions';
import { PanelGroup, Panel } from 'react-bootstrap';

class FrequentlyAskedQuestions extends Component {
    componentDidMount() {
        post('getfaq')
            .then(res => {
                if (res.data.success) {
                    const { faq_details } = res.data;
                    this.props.fetchFaqDetails(faq_details);
                }
            })
            .catch(err => {
                throw new Error('Could not fetch products. Try again later.');
            });
    }
    
    render() {
        const { faqDetails } = this.props;
        return(
            <TemplateWithGoBackButton>
		        <div className="block-title block-position-fixed">
			        <h3>Frequently Asked Questions</h3>
		        </div> 
                <div className="pd-15 mt-20"></div>
                <div className="block pd-15">
                    <PanelGroup accordion>
                        {faqDetails && faqDetails.map((faq, key) => (
                            <Panel eventKey={key}>
                                <Panel.Heading>
                                    <Panel.Title toggle>{faq.faq_question}</Panel.Title>
                                </Panel.Heading>
                                <Panel.Body collapsible dangerouslySetInnerHTML={{ __html: faq.faq_answer }} />
                            </Panel>
                        ))}
                    </PanelGroup>
			        <div className="clearfix"></div>
		        </div>
            </TemplateWithGoBackButton>
        );
    }
};

const mapStateToProps = (state) =>({
    faqDetails: state.faq,
});

const mapActionsToProps = {
    fetchFaqDetails: fetchFaqDetails
};

export default connect(mapStateToProps, mapActionsToProps)(FrequentlyAskedQuestions);