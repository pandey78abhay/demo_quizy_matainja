import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import TemplateWithGoBackButton from '../../template/template-with-goback-button';
import { isLoggedIn } from '../../helper/authentication';
import Swal from 'sweetalert2';
import { post, post_loggedInUser } from '../../http-api';
import './reward-details.css';
import shareIcon from '../../assets/share.png';
import Share from '../../components/share';
import {BASE_URL_IMAGE} from '../../http-api';
import { DetailsSimmer, TitleSimmer, DetailsBannerSimmer } from '../../content-loader';
import { NoDataFound } from '../no-data-found';

class RewardDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rewardDetails: {},
            shareComponent: false,
            noDataFound: false,
        } 
    }

    componentDidMount() {
        this.getDetailsById(this.props.match.params.id);
    }

    openShareComponent = () => {
        this.setState({shareComponent: true});
        document.getElementById('pageContent').classList.add("overflow-hidden");
    } 

    closeShareComponent = () => {
        this.setState({shareComponent: false});
        document.getElementById('pageContent').classList.remove("overflow-hidden");
    }

    getDetailsById = (id) => {
        const payload = new FormData();
        payload.append('reward_id', id);
        post('getrewardbyid', payload)
        .then(res => {
            if(res.data.success) {
                let { each_reward } = res.data;
                this.setState({rewardDetails: each_reward, noDataFound: false});
            }
            if (res.data.error) {
                this.setState({noDataFound: true});
            }
        })
        .catch(err => {
            this.setState({noDataFound: true});
            throw new Error('Could not fetch products. Try again later.');
        });
    }

    buyReward = (reward_id) => {
        if(isLoggedIn()){
            // Authenticated user
            const {rewardDetails} = this.state;
            console.log('api call for bye rewards');
            var form = new FormData();
            form.append("reward_id", reward_id);
            post_loggedInUser('buyreward', form)
            .then(res => {               
                if(res.data.success) {
                    Swal({
                        title: `<i style=color:green>You have got this reward.</i>`,
                        imageUrl: BASE_URL_IMAGE+rewardDetails.reward_image,
                        // imageWidth: 150,
                        // imageHeight: 150,
                        confirmButtonText: "Ok",
                        allowOutsideClick: false,
                        imageClass: 'img-responsive rounded-circle',
                    }).then((result) => {
                        if (result.value) {
                            post_loggedInUser('user_details')
                            .then(res => {
                                console.log(res);
                                const {eachuserdetails} = res.data;
                                localStorage.setItem('user_details', JSON.stringify(eachuserdetails));
                                this.props.history.push({ pathname: '/rewards/my' });
                            })
                            .catch(err => {
                                console.log(err);
                                throw new Error('Could not fetch products. Try again later.');
                            });
                        }
                    })
                    }
                    if(res.data.error) {
                        Swal({
                            title: `<i style=color:red>Oops! You haven't enough coin to bye this reward.</i>`,
                            imageUrl: BASE_URL_IMAGE+reward.reward_image,
                            // imageWidth: 150,
                            // imageHeight: 150,
                            confirmButtonText: "Ok",
                            imageClass: 'img-responsive rounded-circle',
                        })
                    }
                }) 
                .catch(err => {
                    console.log(err);
                    throw new Error('Could not fetch products. Try again later.');
                });


        } else {
            // Not Authenticated user
            this.props.history.push({
                pathname: '/login',
                state: {from: '/rewards/details'}
            });
        }
    }

    render() {
        const {rewardDetails, shareComponent, noDataFound} = this.state;

        if (noDataFound)
            return (<TemplateWithGoBackButton>
                        <NoDataFound />
                    </TemplateWithGoBackButton>);
        return ( 
            <TemplateWithGoBackButton>
                <Fragment>
                <div class="col-xs-12 pd-10">  
                    <div className="rewards-card">
                    <div className="fixed-off">
                    <div className="fixed-off1">
                        <div className="rewards-img">
                            {rewardDetails.reward_image && <img
                                height="150px"
                                src={BASE_URL_IMAGE+rewardDetails.banner_image}
                                style={{width: '100%', objectFit: 'cover', objectPosition: 'top center'}}
                                className="image-responsive"
                            />}
                            {!rewardDetails.reward_image && <DetailsBannerSimmer />}
                            <div className="coin"><strong>{rewardDetails.coin} {rewardDetails.coin<2 ? 'Coin' : 'Coins'}</strong></div>
                            <div className="share-icon">
                                {Object.keys(rewardDetails).length ? <img onClick={()=>this.openShareComponent()} src={shareIcon} />:<img src={shareIcon} />}
                            </div>
                        </div>
                        <div className="pd-10">
                            <div class="col-xs-9 pd-0">
                                <h4 class="mt-0" style={{textTransform: 'capitalize'}}>{rewardDetails.title? rewardDetails.title : <TitleSimmer />}</h4>
                                <div class="info-game-rating">
                                    <span class="info-game-rating">
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                    </span>
                                </div>
                            </div>
                            <div className="col-xs-3 pd-0">
                                {Object.keys(rewardDetails).length ? <p onClick={()=>this.buyReward(rewardDetails.id)} class="price">Tukarkan!</p>:<p class="price diasble">Tukarkan!</p>}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        </div>
                        </div>
                        <div class="col-xs-12 pd-10" style={{marginTop: '230px'}}>
                            { !rewardDetails.description && <DetailsSimmer />}
                            { rewardDetails.description && <div 
                                className="text-desc" 
                                style={{lineHeight: '20px', letterSpacing: '0.5px'}} 
                                dangerouslySetInnerHTML={{ __html: rewardDetails.description }}
                            />}
                        </div>
                    </div>					    		
                </div>
                {shareComponent && <Share closeShareComponent={this.closeShareComponent}/>}
                </Fragment>
            </TemplateWithGoBackButton>
        );
    }
};

export default withRouter(RewardDetails);