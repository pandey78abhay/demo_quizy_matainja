import React, { Component } from 'react';
import classNames from 'classnames';
import InfiniteScroll from 'react-infinite-scroll-component';
import { connect } from "react-redux";
import { updateMyRewards, setMyRewards } from '../../../store/actions';
import '../index.css';
import gold_coin from '../../../assets/gold_coin.png';
import { QuizLoader, AllQuizLoader } from '../../../content-loader';
import { post_loggedInUser, BASE_URL_IMAGE } from '../../../http-api';

class MyRewardsComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			items: Array.from({ length: 20 }),
			page: 0
		}
	}
	componentDidMount() {
		post_loggedInUser('myreward')
			.then(res => {
				if (res.data.success) {
					let { myreward_details } = res.data;
					this.props.setMyRewards(myreward_details); 
				}
			})
			.catch(err => {
				console.log(err);
				throw new Error('Could not fetch products. Try again later.');
			});
	} 
	loadData = () => {
		const { page } = this.state;
		setTimeout(() => {
		this.fetchMoreData(page + 1);
		this.setState({page: page + 1});
		}, 2000);

	}
	fetchMoreData = (page = 0) => {
		var payload = new FormData();
		payload.append("page", page);
		post_loggedInUser('myreward', payload)
			.then(res => {
				if (res.data.success) {
					let { myreward_details } = res.data;
					this.props.updateMyRewards(myreward_details);
				}
			})
			.catch(err => {
				console.log(err);
				throw new Error('Could not fetch products. Try again later.');
			});
	};
    render() {
		const classes = ["my-reward-bg-color-1", "my-reward-bg-color-2"]; 
		const {myRewards} = this.props;
        return(
				// <div id="scrollable" style={{ maxHeight: '260px', overflow: "auto" }}>
				<InfiniteScroll
            			dataLength={myRewards.length}
            			next={this.loadData}
            			hasMore={true}
            			loader={<QuizLoader />}
            			scrollableTarget = "pageContent"
          			>
				{
					myRewards.map((reward, key)=>(
			 			<RewardItem key={key} rewardbgcolor={classes[key % 2]} reward={reward} />
					))
				}
				</InfiniteScroll>
				// </div>
		);
    } 
}; 
 
const mapStateToProps = (state) => ({
	myRewards: state.rewards.my
});

const mapActionsToProps = {
	updateMyRewards: updateMyRewards,
	setMyRewards: setMyRewards
};

export default connect(mapStateToProps, mapActionsToProps)(MyRewardsComponent);


const RewardItem = ({reward, rewardbgcolor}) => {
	return(
		<div className={classNames(rewardbgcolor,'my-reward')}>
            <div className="col-xs-3" style={{paddingLeft: '5px'}}>
                <img src={BASE_URL_IMAGE+reward.reward_image} height="40px" alt=""/> 
            </div>
            <div className="col-xs-6 pd-0">
                <div className="text-hstry" style={{whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', maxWidth: '100%'}}>{reward.title}</div>
            </div>
            <div className="col-xs-3" style={{paddingRight: '0px', paddingLeft: '10px'}}>
				<img src={gold_coin} style={{height: '30px', position: 'relative', zIndex: 1}}/>
				<span class="badge" style={{fontSize: '12px', marginLeft: '-8px', paddingLeft: '10px', borderRadius:'5px'}}>{reward.coin}</span>
            </div>
		</div>
	);
};