import React, { Component } from 'react';
import {RedeemYourRewards, TopRewards} from './lazy-load';
import TemplateWithGoBackButton from '../../template/template-with-goback-button';

class AllRewards extends Component {
    render() {
		console.log(this.props); 
        return(
			 <TemplateWithGoBackButton>
				<div class="block pd-0">
					<div class="pd-10">
						<div class="pd-5">
							<div class="tab-content">
								<div class="columns">
									<div class="column col-12 col-xs-12 pd-0">
										<div class="columns">
											<div class="col-xs-7 pd-0">
												<p><strong>Redeem your reward</strong></p>
											</div>
											<div class="col-xs-5 pd-0">
												&nbsp;
											</div>
										</div>
										<RedeemYourRewards /> 
									</div>
									<div class="column col-12 col-xs-12 pd-0">
										<div class="columns">
											<div class="col-xs-7 pd-0">
												<p><strong>Top Reward</strong></p>
											</div>
											<div class="col-xs-5 pd-0">
												&nbsp;
											</div>
										</div>
										<TopRewards />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		  </TemplateWithGoBackButton>
        );
    }
};

export default AllRewards;