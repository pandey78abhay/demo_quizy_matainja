import React from 'react';
import { Link } from 'react-router-dom';
import LoginHeaderComponent from '../components/login/login-header-component';
import LoginBodyComponent from '../components/login/login-body-component';
import TemplateWithGoBackButton from '../template/template-with-goback-button';

const Login = (props) => {
	return(
		<TemplateWithGoBackButton {...props}>
			<div className="block pt-45 m-20 no-bg">
				<div className="card card-signup">  		
					<LoginHeaderComponent />
					<LoginBodyComponent {...props} />
					<p className="text-center text-red">Don’t have an account? <Link to="/register">Register</Link></p>		  		
				</div>
			</div>
		</TemplateWithGoBackButton>
	);
};

export default Login;