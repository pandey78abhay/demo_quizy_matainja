import React from 'react';
import './style.css';
import noDataFoundImage from '../../assets/no-data.png';
export const NoDataFound = () => (
    <div class="data-not-found" style={{backgroundImage: `url(${noDataFoundImage})`}}>no data found</div>
);