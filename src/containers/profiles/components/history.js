import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroll-component';
import { QuizLoader, AllQuizLoader } from '../../../content-loader';
import { setQuizHistory } from '../../../store/actions';
import classNames from 'classnames';
import './index.css'; 
import Swal from 'sweetalert2';
import { post_loggedInUser } from '../../../http-api';
import { BASE_URL_IMAGE } from '../../../http-api';
class QuizHistory extends Component {
    constructor(props) {
       super(props);
        this.state = {
            page: 0,
            hasMore: true
        }
    }

    componentDidMount() {
        this.fetchQuizHistory();
    }

    loadData = () => {
        const { page } = this.state;
        this.fetchQuizHistory(page + 1); 
        this.setState({page: page + 1});
    }

    fetchQuizHistory = (page) => {
        var payload = new FormData();
        payload.append("page", page);
        post_loggedInUser('getquizhistory', payload)
            .then(res=>{
                if(res.data.success) {
                    let { quiz_history } = res.data;
                    this.props.setQuizHistory(quiz_history);
                }
                if(res.data.error) {
                    this.setState({hasMore: false});
                }
            })
            .catch(err => {
                throw new Error('Could not fetch products. Try again later.');
            });
    }

    viewQuizHistoryDetails = (quiz) => {
        Swal({
            title: '',
            html: `<h3 style="text-align: center;margin-top: 0px;margin-bottom: 10px;text-transform: uppercase;">${quiz.title}</h3>
                  <div style="lext-align: left;">
                    <div class="quiz-history-timestamp">
                        <i class="fa fa-clock-o" aria-hidden="true"></i> ${timestampFormat(quiz.total_time)} <span class="quiz-history-total">Total ${parseInt(quiz.right) + parseInt(quiz.wrong) + parseInt(quiz.not_answer)}</span>
                    </div>
                    <div class="quiz-history-item">Attempt ${parseInt(quiz.right) + parseInt(quiz.wrong)}</div>
                    <div class="quiz-history-item">Not Attempt ${quiz.not_answer}</div>
                    <div class="quiz-history-item quiz-history-item-right">Right ${quiz.right}</div>
                    <div class="quiz-history-item quiz-history-item-wrong">Wrong ${quiz.wrong}</div>
                    <div class="quiz-history-item">Score ${quiz.score}</div>
                  </div>
                  `,
            imageUrl: BASE_URL_IMAGE+quiz.title_image,
            confirmButtonText: "OK",
            imageClass: 'img-responsive rounded-circle',
        });
    }

    render() {
        const { quizHistories } = this.props;
        const { hasMore } = this.state;

        const classes = ["quiz-history-bg-color-1", "quiz-history-bg-color-2"];
        const histories = quizHistories.map((quiz, key) => (
            <div onClick={()=>this.viewQuizHistoryDetails(quiz)} key={key} className={classNames(classes[key % 2], 'quiz-history')}>
                <div className="col-xs-3">
                    <img style={{height: '40px', width: '40px', objectFit: 'cover', objectPosition: 'center'}} src={BASE_URL_IMAGE+quiz.title_image} alt=""/> 
                </div>
                <div className="col-xs-6 pd-0">
                    <div className="text-hstry" style={{whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', maxWidth: '100%'}}>{quiz.title}</div>
                </div>
                <div className="col-xs-3">
                    <div className="text-hstry pull-right">{parseFloat(quiz.accurate).toFixed(1)}%</div>
                </div>
            </div>
        ));
        return(
            <Fragment>
                <div className="block pd-15">
                    {!quizHistories.length?<AllQuizLoader />:
                    <InfiniteScroll
            			dataLength={quizHistories.length}
            			next={this.loadData}
            			hasMore={hasMore}
            			loader={<QuizLoader />}
            			scrollableTarget = "pageContent"
          			>{histories}
				</InfiniteScroll>}
			        <div className="clearfix"></div>
		        </div>
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => ({
    quizHistories: state.quiz.history,
});

const mapActionsToProps = {
    setQuizHistory: setQuizHistory
};

export default connect(mapStateToProps, mapActionsToProps)(QuizHistory);

const timestampFormat = (time) => {
    const min = Math.floor(time/(60*1000));
    time = (time - min*(60*1000));
    const sec = Math.floor(time/1000);
    const milisec = time - sec * 1000;
    return ` ${min>9? min: '0'+min}<sub>m</sub> ${sec>9? sec: '0'+sec}<sub>s</sub> ${milisec>9? parseInt(milisec).toFixed(0): '0'+parseInt(milisec).toFixed(0)}<sub>ms</sub>`;
}