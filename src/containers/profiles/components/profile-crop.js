import React, { Component } from 'react';
import AvatarEditor from 'react-avatar-editor';

class ProfileCrop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scale: 1,
            rotate: 0
        }
    }

    setEditorRef = (editor) => this.editor = editor

    onClickSave = () => {
        // console.log('onClickSave');
        if (this.editor) {
            //const canvas = this.editor.getImage().toDataURL();
            //this.props.setCropImage(canvas);
            const canvasScaled = this.editor.getImageScaledToCanvas().toDataURL();
            this.props.setCropImage(canvasScaled);
        }
    }
    onExitCropComponent = () => {
            this.props.setCropImage(null);
    }

    onHandlerZoom = (event) => {
        // console.log(event.target.value);
        this.setState({
            scale: event.target.value
        });
    }

    onHandlerRotateLeft = () => {
        this.setState((prvState) => ({
            rotate: prvState.rotate - 90
        }))
    }

    onHandlerRotateRight = () => {
        this.setState((prvState) => ({
            rotate: prvState.rotate + 90
        }))
    }

    render() {
        const { scale, rotate } = this.state;
        const { selectedImage } = this.props;
        return(
            <div style={{display: 'table', width: '100%'}}>
                <div style={{display: 'table-row'}}>
                    <div style={{display: 'table-cell', textAlign: 'right', padding: '0px 0px 10px 0px'}}>
                        <span onClick={this.onExitCropComponent} style={{fontSize: '20px', padding: '10px', borderRadius: '50%', color: '#da2323e0'}}>&#x2716;</span>
                    </div>
                </div>
                <div style={{display: 'table-row'}}>
                    <div style={{display: 'table-cell', textAlign: 'center'}}>
                        <AvatarEditor
                            ref={this.setEditorRef}
                            image={selectedImage}
                            width={250}
                            height={250}
                            border={2}
                            borderRadius={2}
                            color={[0, 0, 0, 0.6]} // RGBA
                            scale={scale}
                            rotate={rotate}
                        />
                    </div>
                </div>
                <div style={{display: 'table-row'}}>
                    <div style={{display: 'table-cell', textAlign: 'center', padding: '15px'}}>
                        <div className="slidecontainer">
                            <input 
                                onChange={this.onHandlerZoom}
                                class="slider" 
                                type="range" 
                                step="0.0001"
                                value={scale} 
                                min="1.0000" 
                                max="2.5000"  
                            />
                        </div>
                    </div>
                </div>
                <div style={{display: 'table-row'}}>
                    <div style={{display: 'table-cell', textAlign: 'center'}}>
                        <i 
                            class="fa fa-rotate-left" 
                            onClick={this.onHandlerRotateLeft}
                            style={{fontSize: '24px',margin: '5px', padding: '5px'}}
                        />
                        <i 
                            onClick={this.onHandlerRotateRight}
                            class="fa fa-rotate-right" 
                            style={{fontSize: '24px',margin: '5px', padding: '5px'}}
                        />
                    </div>
                </div>
                <div style={{display: 'table-row'}}>
                    <div style={{display: 'table-cell', textAlign: 'center', padding: '25px'}}>
                        <button
                            onClick={this.onClickSave} 
                            class="pure-material-button-contained"
                            style={{height: '40px', width: '120px'}}
                        >Crop</button>
                    </div>
                </div>
            </div>
        );
    }
};

export default ProfileCrop;