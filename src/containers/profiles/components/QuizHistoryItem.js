import React, {Component, Fragment} from 'react';
import classNames from 'classnames';
// import './index.css';
class QuizHistoryItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }
    onClickOpen = () => {
        this.setState({open: true});
    }
    onClickClose = () => {
        this.setState({open: false});
    }
    render() {
        const base_url = `http://demo.quizy.mobi/assets/uploads/logo/`;
        const classes = ["quiz-history-bg-color-1", "quiz-history-bg-color-2"];
        const {index, quiz} = this.props;
        const {open} = this.state;
        console.log(this.state.open);
        return(
            <div className={classNames(classes[index % 2], 'quiz-history')}>
                <div className="col-xs-2" style={{padding: '0 5px'}}>
                    <img style={{height: '40px', width: '40px', objectFit: 'cover', objectPosition: 'center'}} src={base_url+quiz.title_image} alt=""/> 
                </div>
                <div className="col-xs-10">
                    <div className="text-hstry" style={{
                        whiteSpace: 'nowrap', 
                        overflow: 'hidden', 
                        textOverflow: 'ellipsis', 
                        maxWidth: '100%'
                    }}>{quiz.title}</div>
                    <div className="text-hstry pull-right">{parseFloat(quiz.accurate).toFixed(1)}%</div>
                </div>
                {/* <div className="col-xs-10 pd-0">
                    <div>
                        <div className="text-hstry" style={{
                            whiteSpace: 'nowrap', 
                            overflow: 'hidden', 
                            textOverflow: 'ellipsis', 
                            maxWidth: '100%',
                            textTransform: 'capitalize'
                        }}>{quiz.title}
                        <span style={{float: 'right', marginRight: '30px'}}>{parseFloat(quiz.accurate).toFixed(1)}%</span>
                        </div>
                        <div className="text-hstry pull-right">{parseFloat(quiz.accurate).toFixed(1)}%</div>
                        <div className="slide-left">
                            <i onClick={()=>this.onClickOpen()} class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div className={classNames("history-details", {"history-details-open":open})}> 
                        <div onClick={()=>this.onClickClose()} className={classNames(classes[index % 2], 'slide-right')}><i class="fa fa-angle-double-right" aria-hidden="true"></i></div>
                        <div className="text-hstry" style={{whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', maxWidth: '100%'}}>{quiz.title}</div>
                        <div className="text-hstry pull-right">{parseFloat(quiz.accurate).toFixed(1)}%</div>
                    </div>
                </div> */}
            </div>
        );
    }
};

export default QuizHistoryItem;