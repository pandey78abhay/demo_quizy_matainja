import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Ionicon from 'react-ionicons';
import Swal from 'sweetalert2';
import { isLoggedIn, loggedInUserDetails } from '../../../helper/authentication';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { post_loggedInUser } from '../../../http-api';

import logo from '../../../assets/img/logo.png';
import ProfileCrop from './profile-crop';
import { BASE_URL_PROFILE } from '../../../http-api';

class EditProfile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedImage: null,
			username: '',
			updating: false,
			profileEditor: true
		}
	} 

	fileSelectedHandler = e => {
		const files = e.target.files;
		// console.log(files);
		if (files && files.length > 0) {
			if(this.isValidFile(files)) {
				// console.log('enter');
				const currentFile = files[0];
				const myFileItemReader = new FileReader();
				myFileItemReader.addEventListener("load", () => {
					const myResult = myFileItemReader.result;
					// console.log(myResult);
					this.setState({selectedImage: myResult, profileEditor: false});
				}, false);
				myFileItemReader.readAsDataURL(currentFile);
			} else {
				Swal(
					'Opps Error!',
					'You have uploaded a wrong file!',
					'error'
				);
			}
		}
	}
	isValidFile = (files) => {
		const imageType = ['jpg', 'jpeg', 'png'];
		const file = files[0];
		const fileSyntex = file.name.split('.').pop();
		// console.log(file.name.split('.').pop());
		if(imageType.indexOf(fileSyntex) === -1)
			return false;
		return true;
	}
	onChangeUsername = (e) => {this.setState({username: e.target.value});}

	setInputFile = () => {document.getElementById('profilePic').click();}
	setCropImage = (imgData) => {
		console.log('setCropImage')
		this.setState({selectedImage: imgData, profileEditor: true});
	}
	updateProfile = () => {
		this.setState({updating: true});
		const { username, selectedImage } = this.state;
		var payload = new FormData();
		payload.append('username', username);
		//if file exist
		if(selectedImage)
			payload.append('image', selectedImage);
		console.log(payload);
		post_loggedInUser('updateprofile1', payload)
			.then(res => {
				console.log(res); 
				localStorage.setItem('user_details', JSON.stringify(res.data.profiledetails));
				this.props.history.push({pathname: '/profile'});
				this.setState({updating: false});
			})
			.catch(err => {
				this.setState({updating: false});
				console.log(err);
				throw new Error('Could not fetch products. Try again later.');
			});
	}
	
	componentDidMount() {
		const userDetails = loggedInUserDetails();
		this.setState({username: userDetails.username});
	}
    render() {
		const userDetails = loggedInUserDetails();
		const { username, updating ,selectedImage, profileEditor} = this.state;
        return(
			<React.Fragment>
				<div 
					id="toolbar" 
					className="bluedark toporange botorange"
				>
                    <div className="open-left">
                		<Link to="/profile" className="link back">
							<Ionicon 
								icon="md-arrow-back" 
								fontSize="1em" 
								color="#fff" 
							/>
                		</Link>
            		</div>
            		<div className="logo m-0">
                		<Link to='/'>
                    		<img src={logo} alt="" style={{height: '42px'}}/>
                		</Link>
            		</div>
            		<div className="open-right" data-activates="slide-out-right">
                		<Link to="/quiz/search" seach="true" className="ion-head">
                    		<Ionicon icon="md-search" fontSize="1em" color="#fff" />
                		</Link>
            		</div>
                </div>
                <div className="clearfix"></div>
                <div className="page-content page-content-bg-color">
                    <div className="block-title">
                        <h3>Edit Profile</h3>
                    </div>
                    <div className="block pd-15">
			            <div className="list-block">
							{profileEditor?<div style={{display: 'table', width: '100%'}}>
								<div style={{display: 'table-row'}}>
                    				<div style={{display: 'table-cell', textAlign: 'center'}}>
										<input 
											type="text" 
											className="form-control my-input"
											placeholder="Username" 
											value={username}
											onChange={this.onChangeUsername}
										/>
									</div>
								</div>
								<div style={
									{
										display: 'table-row'
									}
								}>
                    				<div style={
										{
											display: 'table-cell', 
											textAlign: 'center', 
											position: 'relative'
										}
									}>
										<div style={
											{
												width: '170px',
												height: '170px',
												overflow: 'hidden',
												margin: '25px auto 0px auto',
												borderRadius: '50%',
												position: 'relative'
											}
										}>
											<LazyLoadImage
												effect="blur"
												height="140px"
												width="140px"
												src={selectedImage? selectedImage : BASE_URL_PROFILE+userDetails.image}
												style={{borderRadius: '50%'}}
											/>
										</div>
										<button 
											className="material-button-raised" 
											onClick={this.setInputFile}
										>
											<Ionicon 
												icon="md-camera" 
												fontSize="2em" 
												color="#fff" 
												style={
													{
														marginTop: '11px'
													}
												}
											/>
										</button>
										<input 
											id="profilePic" 
											type="file" 
											onChange={this.fileSelectedHandler} 
											accept=".png, .jpg, .jpeg" 
											style={{display: 'none'}} />
									</div>
								</div>
								<div style={{display: 'table-row'}}>
									<div 
										style={
											{
												display: 'table-cell',
												textAlign: 'center',
												padding: '25px'
											}
									}>
										{updating ?
												<div 
													type="button" 
													className="btn btn-primary btn-round" 
													style={{
														padding: '0px 34px',
														height: '40px',
														overflow: 'hidden'
													}}
												>
													<svg class="lds-message" width="50px" height="50px" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style={{background: 'rgba(0, 0, 0, 0) none repeat scroll 0% 0%', marginTop: '-4px'}}>
														<g transform="translate(20 50)">
															<circle cx="0" cy="0" r="8" fill="#ffffff">
																<animateTransform attributeName="transform" type="scale" begin="-0.375s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"/>
															</circle>
														</g>
														<g transform="translate(40 50)">
															<circle cx="0" cy="0" r="8" fill="#ffffff">
																<animateTransform attributeName="transform" type="scale" begin="-0.25s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"/>
															</circle>
														</g>
														<g transform="translate(60 50)">
															<circle cx="0" cy="0" r="8" fill="#ffffff">
																<animateTransform attributeName="transform" type="scale" begin="-0.125s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"/>
															</circle>
														</g>
														<g transform="translate(80 50)">
															<circle cx="0" cy="0" r="8" fill="#ffffff">
																<animateTransform attributeName="transform" type="scale" begin="0s" calcMode="spline" keySplines="0.3 0 0.7 1;0.3 0 0.7 1" values="0;1;0" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite"/>
															</circle>
														</g>
													</svg>
												</div>
												:
												<button disabled={username.length? false: true}
													onClick={this.updateProfile} 
													class="pure-material-button-contained"
													style={{height: '40px', width: '120px'}}
												>Update</button>
										}
									</div>
								</div>
							</div>: <ProfileCrop setCropImage={this.setCropImage} selectedImage={selectedImage}/>}
			            </div>
					
		        </div>
				</div>
				</React.Fragment>
        );
    }
};

export default EditProfile; 