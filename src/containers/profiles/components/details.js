import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { loggedInUserDetails } from '../../../helper/authentication';
import Avatar from 'react-avatar';
import gold_coin from '../../../assets/gold_coin.png';
import { BASE_URL_PROFILE, post_loggedInUser } from '../../../http-api'; 

class ProfileDetails extends Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		post_loggedInUser('userinfoNew')
			.then((res)=>{ 
				localStorage.setItem('user_details', JSON.stringify(res.data.userinfo));
				this.setState({});
			});
	}
    render() {
		const userDetails = loggedInUserDetails();
        return(
            <Fragment>
				<div className="block-title" style={{position: 'relative'}}>
					<h3>Profile</h3>
					<Link to="/profile/edit"
						className="pure-material-button-contained"
						style = {{position: 'absolute', borderRadius: '25px', color: 'white', left: '0px', top: '0px', bottom: '0px', margin: 'auto auto auto 10px'}}
					>
						<i className="fa fa-edit"></i>
					</Link>
					<div style={{position: 'absolute', right: '10px', top: 0, bottom: 0, margin: 'auto'}}>
						<img src={gold_coin} style={{height: '45px', position: 'relative', zIndex: 1}}/>
						<span class="badge" style={
							{
								fontSize: '14px', 
								marginLeft: '-12px',
								padding: '8px 12px 9px 13px',
								borderRadius:'5px', 
								backgroundColor: '#FF4E1A'
							}
						}>{userDetails.coin}</span>
					</div>
				</div>
                <div className="block pd-15">
					<UserDetails userDetails={userDetails} />
			        <div className="col-xs-6 mt-20">
						<button 
							class="pure-material-button-contained"
							style={{height: '45px', width: '158px', backgroundColor: '#115911'}}
						>Unsubscribe</button>
			        </div>
			        <div className="col-xs-6 mt-20">
						<button 
							class="pure-material-button-contained"
							style={{height: '45px', width: '135px'}}
						>Renewal</button>
			        </div>
			        <div className="clearfix"></div>
		        </div>
            </Fragment>
        );
    }
};

export default ProfileDetails;

const UserDetails = ({userDetails}) => (
	 <div className="set">
		<div className="set-img">
			<Avatar
				round={true}
				size="140"
				src={userDetails.image?`${BASE_URL_PROFILE+userDetails.image}`: null} 
				name={userDetails.first_name+' '+userDetails.last_name}
			/>
		</div>
		<div className="set-bar">
			<div className="set-extra">
				<h5 className="desc1">Username: 
					<span>{userDetails.username}</span>
				</h5>
			</div>
			<div className="set-extra">
				<h5 className="desc2">Msisdn: 
					<span>{userDetails.msisdn}</span>
				</h5>
			</div>
			<div className="set-extra">
				<h5 className="desc1">Status: 
					<span>Subscriber</span>
				</h5>
			</div>
		</div>
		<div className="clearfix"></div>
	</div>
);