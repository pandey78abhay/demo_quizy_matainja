import React, { Component } from 'react';
import classNames from 'classnames';

import TemplateWithSlideBar from '../../template/template-with-slide-bar';
import './details.css'; 

import ProfileDetails from './components/details';
import QuizHistory from './components/history';

class Profile extends Component {    
    constructor(props) {
        super(props);
        this.state = {scrollFixed: false}
    }

    componentDidMount() {
        document.getElementById('pageContent').addEventListener('scroll', this.handleScroll);
    }
    
    componentWillUnmount() { 
        document.getElementById('pageContent').removeEventListener('scroll', this.handleScroll);
    }
    
    handleScroll = event => {
        if (event.srcElement.scrollTop>=258)
            this.setState({scrollFixed: true}); 
        else
            this.setState({scrollFixed: false});
    }
    render() {
        const { scrollFixed } = this.state;
        return(
            <TemplateWithSlideBar>
                <ProfileDetails />
                <div className={classNames("block-title", {"scroll-fixed": scrollFixed})}>
                    <h3>Quiz History</h3>
                </div>
                <QuizHistory />
            </TemplateWithSlideBar>
        );
    }
}

export default Profile;