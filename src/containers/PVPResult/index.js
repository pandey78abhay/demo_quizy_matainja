import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import { Link, withRouter } from 'react-router-dom';
import TemplateWithSlideBar from '../../template/template-with-slide-bar';
import Avatar from 'react-avatar';

import { Ellipsis } from 'react-awesome-spinners';
import { post } from '../../http-api';
import { loggedInUserDetails } from '../../helper/authentication';
import 'react-lazy-load-image-component/src/effects/blur.css';
import './result.css';
import { BASE_URL_PROFILE } from '../../http-api';
import shareIcon from '../../assets/share.png';
import avtar from '../../assets/rankimg.png'
import Share from '../../components/share';
import { NoDataFound } from '../no-data-found';
import { DetailsSimmer } from '../../content-loader';
import OneUserResult from './OneUserResult';

class PVPResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            result1: {},
            result2: {},
            noDataFound: false,
            shareComponent: false,
            pairID: this.props.location.state.pairId,
            loading: true,
            waitingForPVPResult: true,
            resultError: false
        }
    }
    openShareComponent = () => {
        this.setState({ shareComponent: true });
        document.getElementById('pageContent').classList.add("overflow-hidden");
    }
    closeShareComponent = () => {
        this.setState({ shareComponent: false });
        document.getElementById('pageContent').classList.remove("overflow-hidden");
    }
    componentDidMount() {
        this.getResultById(this.props.match.params.id);
        if (this.state.waitingForPVPResult) {
            this.interval = setInterval(this.getResultById.bind(this), 5000);
            this.turnOffApiHitTimeout = setTimeout(() => {
                this.setState({ waitingForPVPResult: false })
                clearInterval(this.interval);
                
                this.setState({ resultError: true }, () => {
                    // console.log(this.state.resultError)
                    if (this.state.resultError) {
                        // console.log(this.props)
                        const payload = new FormData();
                        payload.append('pair_id', this.state.pairID);
                        payload.append('category_id', this.props.location.state.categoryId);
                        post('pvp_PairDeActivation', payload)
                            .then(res => {
                                // console.log('pair deactivated')

                            })
                    }
                })
            }, 130000)


        }


    }
    
    componentWillMount() {
        clearInterval(this.interval);
    }
    getResultById = () => {

        this.setState({ loading: true });
        const payload = new FormData();
        payload.append('pair_id', this.state.pairID);
        post('pvp_getresult', payload)
            .then(res => {
                // console.log(this.props.location.state.user_id)
                let { user_id } = this.props.location.state;
                // console.log(user_id)
                if (res.data.success && res.data.pair_result) {
                    if (res.data && res.data.result1.user_id == user_id) {
                        let { result1 } = res.data;
                        let { result2 } = res.data;
                        // console.log(result1, result2, user_id);
                        this.setState({ result1, result2, noDataFound: false, loading: false });
                        clearInterval(this.interval);
                    }
                    else if (res.data && res.data.result2.user_id == user_id) {
                        let { result1 } = res.data;
                        let { result2 } = res.data;
                        // console.log(result1, result2, user_id);
                        this.setState({ result1, result2, loading: false });
                        clearInterval(this.interval);

                    }


                }
                if (res.data.error && res.data.pair_result == 0) {
                    let { result1 } = res.data;
                    // console.log(result1, user_id);
                    this.setState({ result1, loading: false });
                    this.setState({ loading: true });
                }
            })
            .catch(err => {
                console.log(err)
                throw new Error('Could not fetch products. Try again later.');
            });
    }

    timeFormat = (time) => {
        const min = Math.floor(time / 60);
        const sec = time - min * 60;
        return `  ${min}m ${(sec).toFixed(3)}s`;
    }
    homeButton = () => {

        this.props.history.push('/')
    }

    render() {
        const { result1, result2, shareComponent, noDataFound, loading, resultError } = this.state;
        if (noDataFound)
            return (
                <TemplateWithSlideBar>
                    <NoDataFound />
                </TemplateWithSlideBar>
            );
        if (loading && !resultError)
            return (
                
                <div style={{width:'100%',height:'100%',textAlign: 'center',
                paddingTop:'50%'}}>
                <div ><Ellipsis /></div>
                <div style={{
                    color: '#4a774a',
                    fontSize: '20px',
                    padding: '15px',
                }}>Game not completed, waiting for 2nd player...</div>
                </div>
                
            )
        if (loading && resultError) {
            return <OneUserResult result1={result1} />
        }
        if (!loading)
            return (
                <Fragment>
                    <TemplateWithSlideBar>
                        <div className="block-title">
                            <h3>Quiz Result</h3>
                        </div>
                        <div class="share-result">
                            <div className="share-icon-result" onClick={() => this.openShareComponent()}>
                                <span>Share</span>
                                <img src={shareIcon} />
                            </div>
                        </div>
                        <div className="block pd-15">
                            <div className="quiz-info">
                                <h5>
                                    PVP Quiz: <span style={{ textTransform: 'capitalize' }}>
                                        {result1.category_title}
                                    </span>
                                    <span className="pull-right"><i className="fa fa-clock"></i>
                                        {this.timeFormat(result1.total_time)}
                                    </span>
                                </h5>
                            </div>
                            <div className="set">
                                <div className="set-img">
                                    <Avatar
                                        round={true}
                                        size="140"
                                        src={result1.image ? `${BASE_URL_PROFILE + result1.image}` : null}
                                        name={'Goutam Das'}
                                    />
                                </div>
                                <div className="set-bar">
                                    <div className="set-extra">
                                        <h5 className="desc1">Right Answer: <span>
                                            {result1.right}
                                        </span></h5>
                                    </div>
                                    <div className="set-extra">
                                        <h5 className="desc2">Wrong Answer: <span>
                                            {result1.wrong}
                                        </span></h5>
                                    </div>
                                    <div className="set-extra">
                                        <h5 className="desc1">Accurate: <span>
                                            {(parseFloat(result1.accurate)).toFixed(2)}%
                                    </span></h5>
                                    </div>
                                </div>
                            </div>
                            <h1 style={{ fontWeight: '800' }}>{result1.first_name}</h1>
                            <div className="score">
                                <div className="score-desc-left">Score

                            </div>
                                <div className="score-desc-right">{result1.score}</div>
                            </div>
                            <div className="quiz-info">
                                <h5>
                                    PVP Quiz: <span style={{ textTransform: 'capitalize' }}>
                                        {result1.category_title}
                                    </span>
                                    <span className="pull-right"><i className="fa fa-clock"></i>
                                        {this.timeFormat(result2.total_time)}
                                    </span>
                                </h5>
                            </div>
                            <div className="set">
                                <div className="set-img">
                                    <Avatar
                                        round={true}
                                        size="140"
                                        src={result2.image ? `${BASE_URL_PROFILE + result2.image}` : null}
                                        name={'Goutam Das'}
                                    />

                                </div>
                                <div className="set-bar">
                                    <div className="set-extra">
                                        <h5 className="desc1">Right Answer: <span>
                                            {result2.right}
                                            {/* 3 */}
                                        </span></h5>
                                    </div>
                                    <div className="set-extra">
                                        <h5 className="desc2">Wrong Answer: <span>
                                            {result2.wrong}
                                        </span></h5>
                                    </div>
                                    <div className="set-extra">
                                        <h5 className="desc1">Accurate: <span>
                                            {(parseFloat(result2.accurate)).toFixed(2)}%
                                    </span></h5>
                                    </div>
                                </div>
                            </div>
                            <h1 style={{ fontWeight: '800' }}>{result2.first_name}</h1>
                            <div className="score">
                                <div className="score-desc-left">Score

                            </div>
                                <div className="score-desc-right">{result2.score}</div>
                            </div>

                            {/* <Score score={result.score} bestScore={result.best_score} /> */}
                        </div>
                        <div className="clearfix"></div>
                        <div className="bottom-fixed-status">
                            <ul className="list-inline">
                                {/* <li className="bottom-button">
                                    <a onClick={() => this.props.history.goBack()}>
                                        <div><i class="fas fa-undo-alt"></i></div>Attempt Again
                                    </a>
                                </li> */}
                                <li className="bottom-button">
                                    <Link to={{ pathname: '/' }}>
                                        <div><i class="fas fa-home"></i></div>Home
                                    </Link>
                                </li>
                                <li className="bottom-button">
                                    {/* <Link to={`/leaderboard/${result.category_id}`}> */}
                                    <Link to=''>
                                        <div><i class="fas fa-trophy"></i></div>Leader Board
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </TemplateWithSlideBar>
                    {/* {shareComponent && <Share closeShareComponent={this.closeShareComponent}/>} */}
                </Fragment>
            );
    }
};
const mapStateToProps = (state) => ({
    //   playedQuestions: state.question.played,
    //   answer: state.answer  
});

const mapActionsToProps = {

};

export default connect(mapStateToProps, mapActionsToProps)(withRouter(PVPResult));

const Score = ({ score, bestScore }) => (
    <div className="score-best-score">
        <div>
            <div>
                <div>Score<span>{score}</span></div>
                <div>Best Score<span>{bestScore}</span></div>
            </div>
        </div>
    </div>
);