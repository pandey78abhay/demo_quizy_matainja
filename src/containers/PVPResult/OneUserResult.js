import React from 'react';
import TemplateWithSlideBar from '../../template/template-with-slide-bar';
import Avatar from 'react-avatar';
import { Link, withRouter } from 'react-router-dom';
import { BASE_URL_PROFILE } from '../../http-api';
import shareIcon from '../../assets/share.png';

const timeFormat = (time) => {
    const min = Math.floor(time / 60);
    const sec = time - min * 60;
    return `  ${min}m ${(sec).toFixed(3)}s`;
}
const openShareComponent = () => {
    this.setState({ shareComponent: true });
    document.getElementById('pageContent').classList.add("overflow-hidden");
}

// const closeShareComponent = () => {
//     this.setState({ shareComponent: false });
//     document.getElementById('pageContent').classList.remove("overflow-hidden");
// }
const OneUserResult = (props) => {


    const homeButton = () => {

        props.history.push('/')
    }


    const { result1 } = props;
    return (
        <TemplateWithSlideBar>
            <div className="block-title">
                <h3>Quiz Result</h3>
            </div>
            <div class="share-result">
                <div className="share-icon-result" onClick={() => openShareComponent()}>
                    <span>Share</span>
                    <img src={shareIcon} />
                </div>
            </div>
            <div className="quiz-info" style={{ padding: '5% 0', textAlign: 'center' }}>
                <h4 style={{ textAlignLast: 'center' }}>
                    <span style={{ textTransform: 'capitalize' }}>
                        Ops!!!Opponent not completed the quiz and Quit the quiz
                                </span>


                </h4>

                <div>
                    <button
                        onClick={homeButton}
                        className="pure-material-button-contained"
                        style={{ textTransform: 'capitalize', top: '15px', letterSpacing: '0.5px', color: '#fff', backgroundColor: 'rgb(50, 205, 50)', }}
                    ><span style={{ paddingRight: '5px' }}>&#9658;</span>Home</button>
                </div>


            </div>

            <div className="block pd-15">
                <div className="quiz-info">
                    <h5>
                        PVP Quiz: <span style={{ textTransform: 'capitalize' }}>
                            {result1.category_title}
                        </span>
                        <span className="pull-right"><i className="fa fa-clock"></i>
                            {timeFormat(result1.total_time)}
                        </span>
                    </h5>
                </div>
                <div className="set">
                    <div className="set-img">
                        {/* {result &&  */}
                        <Avatar
                            round={true}
                            size="140"
                            src={result1.image ? `${BASE_URL_PROFILE + result1.image}` : null}
                            name={'Goutam Das'}
                        />
                        {/* } */}
                    </div>
                    <div className="set-bar">
                        <div className="set-extra">
                            <h5 className="desc1">Right Answer: <span>
                                {result1.right}
                            </span></h5>
                        </div>
                        <div className="set-extra">
                            <h5 className="desc2">Wrong Answer: <span>
                                {result1.wrong}
                            </span></h5>
                        </div>
                        <div className="set-extra">
                            <h5 className="desc1">Accurate: <span>
                                {(parseFloat(result1.accurate)).toFixed(2)}%
                                    </span></h5>
                        </div>
                    </div>
                </div>
                <h1 style={{ fontWeight: '800' }}>{result1.first_name}</h1>

            </div>
            <div className="clearfix"></div>
            <div className="bottom-fixed-status">
                <ul className="list-inline">
                    
                    <li className="bottom-button">
                        <Link to={{ pathname: '/' }}>
                            <div><i class="fas fa-home"></i></div>Home
                                    </Link>
                    </li>
                    <li className="bottom-button">
                        {/* <Link to={`/leaderboard/${result.category_id}`}> */}
                        <Link to=''>
                            <div><i class="fas fa-trophy"></i></div>Leader Board
                                    </Link>
                    </li>
                </ul>
            </div>
        </TemplateWithSlideBar>





    )
}
export default withRouter(OneUserResult);