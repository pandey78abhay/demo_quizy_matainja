import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import TemplateWithGoBackButton from '../template/template-with-goback-button';
import { isLoggedIn } from '../helper/authentication';
class Register extends Component {
    render() {
		// if(isLoggedIn)
		// 	return(<Redirect to="/" />);
        return(
			<TemplateWithGoBackButton {...this.props}>
            <div className="block pt-45 m-20 no-bg">
				<div className="card card-signup">
				    <form className="form">
					    <div className="header header-warning text-center">
					        <h4 className="card-title">Choose Your Purchase</h4>
					    </div>
					    <div className="col-xs-12">
						    <a href="" className="btn-lrg bg-1">
							    <div className="row">
								    <div className="col-xs-1">●</div>
								    <div className="col-xs-10">
									    <p className="mb-0">Subscribe</p>
								    </div>
								    <div className="clearfix"></div>
							    </div>
						    </a>
						    <a href="" className="btn-lrg bg-2 mt-10">
							    <div className="row">
								    <div className="col-xs-1">●</div>
								    <div className="col-xs-10">
									    <p className="mb-0">Buy One Time Only</p>
								    </div>
								    <div className="clearfix"></div>
							    </div>
						    </a>
					  	    <Link to="/login" className="btn btn-primary btn-round mt-15 wd100 mb-10">
							    <p className="mb-0 fs-18">Sign In</p>
						    </Link>
					    </div>
					    <p className="text-center text-red mt-10">Already have an account? <Link to="/login">Sign In</Link>
					  </p>
				  </form>
				</div>
			</div>
			</TemplateWithGoBackButton>
        );
    }
};

export default Register;