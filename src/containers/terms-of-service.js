import React, { Component } from 'react';
import { post } from '../http-api';
import TemplateWithGoBackButton from '../template/template-with-goback-button';
import './terms-of-service.css';
export default class TermsOfService extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content: null
        };
    }
    componentDidMount() {
        const payload = new FormData();
        payload.append("page", "TermsOfServices");
        post('getpage', payload)
            .then(res => {
                this.setState({content:res.data.page_details[0].content});
            });
    }
    render() {
        const { content } = this.state;
        return(
            <TemplateWithGoBackButton {...this.props}>
		        <div className="block-title block-position-fixed">
			        <h3>Terms Of Service</h3>
		        </div>
                <div className="pd-10 mt-10"></div>
                <div 
                    className="block pd-15" 
                    style={{overflowY: 'auto'}} 
                    dangerouslySetInnerHTML={{ __html: content }}
                />
            </TemplateWithGoBackButton>
        );
    }
};