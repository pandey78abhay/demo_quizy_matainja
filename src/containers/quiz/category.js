import React, { Component } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { CategoryGroupLoader, AllCategoryGroupLoader } from '../../content-loader';
import LatestQuizSlider from '../../components/quiz/latest-quiz-slider';
import TemplateWithSlideBar from '../../template/template-with-slide-bar';
import { post } from '../../http-api';

class QuizCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            groups: [],
            page: 0,
            hasMore: false,
            loader: true
        } 
    }
    componentDidMount() {
        this.fetchGroups();
    }
    fetchGroups(page = 0){
        var payload = new FormData();
        payload.append("page", page);
        post('getcategoriesbygroup', payload)
            .then(res => {
                const {groups} = res.data;
                if(res.data.success)
                    this.setState((prevState)=>({
                        groups: [...prevState.groups, ...groups], 
                        hasMore: res.data.hasMore,
                        loader: false
                    }));
            })
            .catch(err => {
                throw new Error('Could not fetch products. Try again later.');
            });
    } 
    loadData = () => {
        const { page } = this.state;
        setTimeout(() => {
            this.fetchGroups(page + 1);
            this.setState({page: page + 1});
        }, 2000); 

    }
    render() {
        const { groups, hasMore, loader } = this.state;

        return(
            <TemplateWithSlideBar>
                {/* <div id="scrollable" style={{ maxHeight: "550px", overflow: "auto" }}> */}
                    {loader? <AllCategoryGroupLoader />:
                        <InfiniteScroll 
                            dataLength={groups.length} 
                            next={this.loadData} 
                            hasMore={hasMore} 
                            loader={<CategoryGroupLoader />} 
                            scrollableTarget="pageContent"
                        >
                            {groups.map((group, key) => (
                                <GroupItem
                                    key={key}
                                    group_name={group.group_name}
                                    categories={group.categories}
                                />
                            ))}
                        </InfiniteScroll>
                    }
                {/* </div> */}
            </TemplateWithSlideBar> 
        );
    }
};
export default QuizCategory; 

const GroupItem = ({group_name, categories}) => (
    <div>
        <div className="block-title">
            <h3>{group_name}</h3>
        </div>
        <div className="block pd-15">
            <LatestQuizSlider latestQuiz={categories} />
        </div>
    </div>
);