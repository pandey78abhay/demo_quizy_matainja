import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import { Link, withRouter } from 'react-router-dom';
import TemplateWithSlideBar from '../../template/template-with-slide-bar';

import { loggedInUserDetails } from '../../helper/authentication';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { post } from '../../http-api';
import img_noData from '../../assets/no-data.png';
import './result.css';
import Score from './score';

class Result extends Component {
    constructor(props) {
        super(props);
        this.state = {
            result: {},
            noData: false,
        }
    }
    componentDidMount() {
        this.getResultById(this.props.match.params.id);
    }
    getResultById = (id) => {
        const payload = new FormData();
        payload.append('result_id', id);
        post('getresult', payload)
        .then(res => {
            if(res.data.success) {
                let { result } = res.data;
                console.log(result);
                this.setState({result});
            }
            if (res.data.error) {
                this.setState({noData: true});
            }
        })
        .catch(err => {
            throw new Error('Could not fetch products. Try again later.');
        });
    }
    
    timeFormat = (time) => {
        const min = Math.floor(time / 60);
        const sec = time - min*60;
        return `  ${min}m ${(sec).toFixed(3)}s`;
    }

    render() {
        const { result, noData } = this.state;
        console.log(result);
        const base_url = `http://demo.quizy.mobi/assets/uploads/profiles/`;
        // USER Details
        const userDetails = loggedInUserDetails();
        
        return(
            <TemplateWithSlideBar>
                <div className="block-title">
                    <h3>Quiz Result</h3>
                </div>
                <div className="block pd-15">
                {!noData ? <Fragment>
                    <div className="quiz-info">
				        <h5>
                            Quiz: {result.category_title}
					        <span className="pull-right">
                                <i className="fa fa-clock"></i> 
                                {this.timeFormat(result.total_time)}
					        </span>
				        </h5>
			        </div>
			        <div className="set">
                        <div className="set-img">
                            <LazyLoadImage
                                effect="blur"
                                height="140px"
                                width="140px"
                                src={base_url+userDetails.image}
                                style={{borderRadius: '50%'}}
                            />
                        </div>
                        <div className="set-bar">
                            <div className="set-extra">
                                <h5 className="desc1">Right Answer: <span>{result.right}</span></h5>
                            </div>
                            <div className="set-extra">
                                <h5 className="desc2">Wrong Answer: <span>{result.wrong}</span></h5>
                            </div>
                            <div className="set-extra">
                                <h5 className="desc1">Accurate: <span>{(parseFloat(result.accurate)).toFixed(2)}%</span></h5>
                            </div> 
                        </div>
			        </div>
                       <div style={{ overflow: '-webkit-paged-x'}}>
                <div style={{margin: '20px 0px 0', padding: '20px 0px 10px'}}>
                    <div style={{position: 'relative'}}>
                        <div style={
                            {
                                padding: '10px',
                                backgroundColor: '#128BB4',
                                color: 'white',
                                width: '50%',
                                float: 'left'
                            }
                        }>Score <span style={{paddingLeft: '10px'}}>{result.score}</span></div>
                         <div style={
                            {
                                padding: '10px',
                                backgroundColor: '#128BB4',
                                color: 'white',
                                textAlign: 'right'
                            }
                        }>Best Score <span style={{float: 'right', paddingLeft: '10px'}}>{result.score}</span></div>
                    </div>
                </div>
            </div>
                    </Fragment>
                    :<div className="no-data-found">
                        <img src={img_noData} />
                    </div>}
                    <div style={{
                        borderTop: '1px solid #ff4e1a',
                        borderBottom: '1px solid #ff4e1a',
                        borderRight: '1px solid rgb(255, 78, 26)'
                    }}>
                        <span className="footer-text" style={
                            {
                                backgroundColor: '#ff4e1a',
                                display: 'inline-block',
                                lineHeight: '39px',
                                padding: '0 10px',
                                letterSpacing: '0.5px',
                                color:'#fff !important'
                            }
                        }>Share this:</span>
                        <div className="social-media-icons">
                            <a href="" target="_blank" className="medium-fb" style={
                                {
                                    display: 'inline-block',
                                    padding: '0px',
                                    textAlign: 'center',
                                    marginRight: '15px'
                                }
                            }>
                                <span className="social-icon text-black">
                                   <i className="fab fa-facebook-f"></i>
                                </span>
                            </a>
                            <a href="" target="_blank" className="medium-twitter" style={
                                {
                                    display: 'inline-block'
                                }
                            }>
                                <span className="social-icon text-black">
                                    <i className="fab fa-twitter"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                    {/* {!noData && <div className="block pd-10 text-center">
                        <a onClick={() => this.props.history.goBack()}
                            class="pure-material-button-contained"
                            style={{height: '40px', width: '165px', textTransform: 'capitalize', textDecoration: 'none', fontSize: '15px', color: 'white'}}
                        >Attempt Again</a>
                    </div>}
                    <div className="block pd-10 text-center">
                        <Link to={{
                            pathname: '/',
                        }}
							class="pure-material-button-contained"
							style={{height: '40px', width: '165px', textTransform: 'capitalize', textDecoration: 'none', fontSize: '15px', color: 'white'}}
						>Back to Home</Link>
                    </div>
                    {!noData&&<div className="block pd-10 text-center">
                        <Link to={`/leaderboard/${result.category_id}`}
							class="pure-material-button-contained"
							style={{height: '40px', width: '165px', textTransform: 'capitalize', textDecoration: 'none', fontSize: '15px', color: 'white'}}
						>Leader Board</Link>
                    </div>} */}
			        <div className="clearfix"></div>
                    <div className="bottom-fixed-status">
                        <ul className="list-inline">
                            <li className="bottom-button">
                                <a onClick={() => this.props.history.goBack()}>
                                    <div><i class="fas fa-undo-alt"></i></div>Attempt Again
                                </a>
                            </li>
                            <li className="bottom-button">
                                <Link to={{pathname: '/'}}>
                                    <div><i class="fas fa-home"></i></div>Home
                                </Link>
                            </li>
                            <li className="bottom-button">
                                <Link to={`/leaderboard/${result.category_id}`}>
                                    <div><i class="fas fa-trophy"></i></div>Leader Board
                                </Link>
                            </li>
                        </ul>
                    </div>
		        </div>
            </TemplateWithSlideBar>
        );
    }
};
const mapStateToProps = (state) => ({
  playedQuestions: state.question.played,
  answer: state.answer  
});

const mapActionsToProps = {
    
};

export default connect(mapStateToProps, mapActionsToProps)(withRouter(Result));