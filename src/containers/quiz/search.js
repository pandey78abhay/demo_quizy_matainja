import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import axios from 'axios';
import TemplateWithSlideBar from '../../template/template-with-slide-bar';
class Search extends Component {
    constructor(props) {
        super(props);
        const queryParams = new URLSearchParams(this.props.location.search);
        this.state = {
            search: !!queryParams.get('q') ? queryParams.get('q') : '',
            searchResult: [] ,
            status: null
        };
    }
    search = (search) => {
        var form = new FormData();
        form.append("title", search);
        axios({
                "async": true,
                "crossDomain": true,
                "url": `http://demo.quizy.mobi/Api/searchcategory`,
                "method": "POST",
                "headers": {
                    "Authorization": "Basic YWRtaW46MTIzNA==",
                    "X-API-KEY": "CODEX@123",
                    "cache-control": "no-cache",
                },
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
                "data": form
            })
            .then(res => {
                if(res.data.success) {
                    const { search_details } = res.data;
                    this.setState({searchResult: search_details, status: 'DATA_FOUND'});
                }
                if(res.data.error) {
                    this.setState({searchResult: [], status: 'DATA_NOT_FOUND'});   
                }
                
            })
            .catch(err => {
                throw new Error('Could not fetch products. Try again later.');
            });
    }

    onChangeInput = (e) => {
        this.setState({search: e.target.value});
    }
    onClickSearch = () => {
        const { search } = this.state;
        this.props.history.replace({
                pathname: '/quiz/search',
                search: 'q='+search
            });
        this.search(search);
    }
    componentDidMount() {
        const queryParams = new URLSearchParams(this.props.location.search);
        if (queryParams.get('q')) {
            this.search(queryParams.get('q'));
        }
    } 
    
    render() {
        const base_url = `http://demo.quizy.mobi/assets/uploads/logo/`;
        const { search, searchResult, status } = this.state;
        let searchResultDetails = null;
        if(searchResult.length > 0) {
            searchResultDetails = searchResult.map((value, key) => (
                <Link to={{pathname: '/quiz/details', state: {quiz: value}}} key={key} className="hstry1">
                    <div className="col-xs-3">
                        <img src={base_url+value.title_image} height="40px" alt=""/> 
                    </div>
                    <div className="col-xs-9 pd-0">
                        <div className="text-hstry" style={{fontSize: '16px', lineHeight: '16px', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', maxWidth: '100%'}}>{value.title}</div>
                        <div style={{color: 'lightslategrey', paddingTop: '3px'}}>{value.total_question} Questions</div>
                    </div>
                </Link>
            ));
        }
        return(
            <TemplateWithSlideBar>
		        <div className="block-title">
			        <h3>Search</h3>
		        </div>
		        <div className="block pd-15">		
			        <div className="container">
                        <div className="row">
                            <div className="well" style={{marginBottom: '15px'}}>
                            {!searchResult.length?		
                                <fieldset>
                                    <div className="form-group">
                                        <input onChange={this.onChangeInput} type="text" className="form-control" name="query" id="query" placeholder="" value={search}/>
                                    </div>
                                    <div className="text-center">
                                        <button disabled={!search.trim().length} onClick={this.onClickSearch} className="btn btn-primary btn-round">Search</button>
                                    </div>
                                </fieldset>:
                                <fieldset>
                                    <div className="text-center">
                                        <input onChange={this.onChangeInput} type="text" className="form-control" name="query" id="query" placeholder="" style={{display: 'inline-block', width: 'auto', backgroundPosition: 'center bottom, center calc(100% - 0px)'}} value={search}/>
                                        <button disabled={!search.trim().length} onClick={this.onClickSearch} className="btn btn-primary btn-round" style={{padding: '6px 12px', marginLeft: '10px'}}>Search</button>
                                    </div>
                                </fieldset>}
                            </div>
                        </div>
                        {status==='DATA_FOUND'?
                            <div className="row">
                                <div className="well" style={{marginBottom: '15px'}}>
                                    {searchResultDetails}
                                </div>
                            </div>
                        :null}
                        {status==='DATA_NOT_FOUND'?
                            <div className="row">
                                <div className="well" style={{marginBottom: '15px'}}>
                                    <div className="text-center">No data found!</div>
                                </div>
                            </div>
                        :null}
                    </div>
			        <div className="clearfix"></div>
		        </div>
            </TemplateWithSlideBar>
        );
    }
};

export default withRouter(Search);