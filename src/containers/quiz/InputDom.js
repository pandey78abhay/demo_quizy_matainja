import React, { Component } from 'react';
import { MDBInput } from "mdbreact";
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
class InputDom extends Component {
    constructor(props) {
        super(props);
        this.state = {
            answer: ''
        }
    }
     parentCall = () => {
        if (this.state.answer === '') {
            Swal.fire(
                'Okay',
                'You can not submit empty value',
                'error'
              )
        }
        else{
            console.log(this.state.answer)
        this.props.submitAnswer(this.state.answer);
        }
        
        // await this.setState({answer:''})
    }
    render() {
        let inputDom;

        inputDom = (<div style={{ display: 'flex', padding: '28px' }}>
            {console.log(this.state.answer)}
            <MDBInput hint="Write your answer" name="answer" onChange={e => this.setState({ answer: e.target.value })} />

            <Button bsStyle="info" style={{ height: '35px', width: '75px' }} onClick={this.parentCall}>Submit</Button>

        </div>)
        return (
            <div>{inputDom}</div>
        )
    }


}
export default InputDom;