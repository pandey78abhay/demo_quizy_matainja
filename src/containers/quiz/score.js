import React, {Component, Fragment} from 'react';

class Score extends Component {
    render() {
        const {score} = this.props;
        return(
            <div style={{ overflow: '-webkit-paged-x'}}>
                <div style={{margin: '20px', padding: '20px 0px'}}>
                    <div style={{position: 'relative'}}>
                        <div style={
                            {
                                float: 'left',
                                width: '100%',
                                position: 'absolute',
                                padding: '10px',
                                backgroundColor: '#128BB4',
                                color: 'white'
                            }
                        }>Your Score <span style={{float: 'right'}}>{score}</span></div>
                        {/* <div style={
                            {
                                float: 'right',
                                background: '#128BB4',
                                color: '#fff',
                                padding: '25px',
                                borderRadius: '100%',
                                position: 'absolute',
                                right: '-11px',
                                top: '-15px'
                            }
                        }>{score}</div> */}
                    </div>
                </div>
            </div>
        );
    }
};

export default Score;