import React, {Component, Fragment} from 'react';


import { withRouter } from 'react-router-dom';
import closeIcon from '../../assets/close-icon.png';

import QuizPlayTemplate from './components/QuizPlayTemplate';

import {Animated} from "react-animated-css";
import PlayingQuiz from './playing-quiz';

import Swal from 'sweetalert2';
import './quiz-details.css';

import QuizDetailsComponent from './QuizDetails';

class QuizDetails extends Component {
     
    constructor(props) {
        super(props);
        this.state = {
            playQuiz: false,
        }
    }


    playQuiz = () => {
        this.setState({playQuiz: true});
    }

    quitQuiz = () => {
        Swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.setState({playQuiz: false});
            }
        })   
    }

    render() {
        const { playQuiz } = this.state;
        console.log(playQuiz)
        return(
            <Fragment>
                    <QuizDetailsComponent playQuiz={this.playQuiz}/>
                     
                    {playQuiz && <Animated style={{position: 'absolute',right: 0,left: 0,top: 0,zIndex: 9999}} animationIn="fadeInUp" animationOut="fadeOutDown" isVisible={playQuiz}><QuizPlayTemplate>
                        <div class="block-title"><h3>Quiz Play</h3></div>
                            <PlayingQuiz />
                            <div class="clearfix"></div>
                            <div style={
                            {
                                position: 'fixed', 
                                bottom: '-60px',
                                right: '-60px',
                                color: '#fff',
                                width: '100px',
                                height: '100px',
                                borderRadius: '36%',
                                backgroundColor: '#8784a1',
                                cursor: 'pointer',
                                lineHeight: '45px',
                                paddingLeft: '18px',
                            }
                        }>
                            <img onClick={this.quitQuiz} src={closeIcon} />
                        </div>
                    </QuizPlayTemplate></Animated>}
            </Fragment>  
        );
    }
};

export default withRouter(QuizDetails);


