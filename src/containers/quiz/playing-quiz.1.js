import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import classNames from 'classnames';
import 'prevent-pull-refresh';
var shuffle = require('shuffle-array');
import Swal from 'sweetalert2';
import { MDBInput } from "mdbreact";
import { Button } from 'react-bootstrap';
import IframeComponent from './IframeComponent';
import Mp3Component from './Mp3Component';
import { withRouter } from 'react-router-dom';
import Loader from '../../components/loader/spiner';
import SubmitLoader from './submit-loader';
import Timer from '../../components/timer';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { selectQuestion, playedQuiz, fetchAnswerByCatId } from '../../store/actions';
import { post_loggedInUser } from '../../http-api';
import DisableBackButton from '../diasble-back-button';

import { CSSTransition, TransitionGroup } from "react-transition-group";
import './playing-quiz.css';

class PlayingQuiz extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quizSubmitting: false,
            answer: '',
            submit: false
        }
    }
    componentDidMount() {
        document.body.classList.add("prevent-pull-refresh");
    }

    componentWillUnmount() {
        document.body.classList.remove("prevent-pull-refresh");
    }

    onChooseOption = (option_choose) => {
        const { selectedQuestion, selected, noOfQuestion } = this.props;
        let option_choose_time = document.getElementById("Progress1").innerHTML;

        if (option_choose === '')
            option_choose_time = parseInt(60);

        if (selected === noOfQuestion) {
            //Submit Quiz
            const { answer_id, points } = selectedQuestion;
            this.setState({ quizSubmitting: true });

            const payload = new FormData();
            payload.append("category_id", this.props.match.params.id);
            let { playedquiz } = this.props;
            let userAnseredMatched = '';
            payload.append("playedquiz", JSON.stringify([...playedquiz, Object.assign({}, { userAnseredMatched, answer_id, points, option_choose, option_choose_time })]));

            post_loggedInUser('submitquiz', payload)
                .then(res => {
                    if (res.data.success) {
                        this.props.history.push({
                            pathname: `/quiz/result/${res.data.last_id}`
                        });
                    }
                })
                .catch(err => {
                    throw new Error('Could not fetch products. Try again later.');
                });
        } else {
            // NEXT QUIZ
            console.log(option_choose_time)
            const { answer_id, points } = selectedQuestion;
            let userAnseredMatched = '';
            this.props.playedQuiz(Object.assign({}, { userAnseredMatched, answer_id, points, option_choose, option_choose_time }));
            this.props.selectQuestion(selected + 1);
        }
    }
    submitAnswer = (ans) => {
        console.log(this.props.answer)
        const { selectedQuestion, selected, noOfQuestion, answerList } = this.props;
        console.log(ans)
        console.log(answerList)
        console.log(this.props)
        // let anserMatchedArray = answerList.filter(answer => (answer.option_name == ans.toUpperCase()))
        // anserMatchedArray.push(answerList.filter(answer=>(answer.quiz_id==selectedQuestion.quiz_id)))
        let answerCheckInArray = answerList.some(answer => (answer.option_name == ans.toUpperCase()))
        // console.log(anserMatchedArray.length)
        // console.log(anserMatchedArray)
        // let anserExist=anserMatchedArray.answer.


        let option_choose_time = document.getElementById("Progress1").innerHTML;
        console.log(selected)
        const { quiz_id, answer_id, points } = selectedQuestion;
        if (ans === '') {
            Swal.fire(
                'Okay',
                'You can not submit empty value',
                'error'
              )
        }
        if (ans !== '') {
            if (selected == noOfQuestion) {
                console.log(option_choose_time)
                //Submit Quiz
                const { answer_id, points } = selectedQuestion;
                this.setState({ quizSubmitting: true });

                const payload = new FormData();
                payload.append("category_id", this.props.match.params.id);
                let { playedquiz } = this.props;
                let option_choose = "";
                console.log(anserMatchedArray.length)
                if (answerCheckInArray) {
                    let anserMatchedArray =[];
                    anserMatchedArray=(answerList.filter(answer => (answer.option_name == ans.toUpperCase())))
                    console.log(anserMatchedArray[0].option_number)
                    // console.log(answerList.option_number)
                    // console.log(option_choose)
                    let userAnseredMatched = anserMatchedArray[0].option_number
                    payload.append("playedquiz", JSON.stringify([...playedquiz, Object.assign({}, { userAnseredMatched, answer_id, points, option_choose, option_choose_time })]));
                    post_loggedInUser('submitquiz', payload)
                        .then(res => {
                            if (res.data.success) {
                                this.props.history.push({
                                    pathname: `/quiz/result/${res.data.last_id}`
                                });
                            }
                        })
                        .catch(err => {
                            throw new Error('Could not fetch products. Try again later.');
                        });
                }
                else {
                    let option_choose = "";
                    let userAnseredMatched = 'no'
                    payload.append("playedquiz", JSON.stringify([...playedquiz, Object.assign({}, { userAnseredMatched, answer_id, points, option_choose, option_choose_time })]));
                    post_loggedInUser('submitquiz', payload)
                        .then(res => {
                            if (res.data.success) {
                                this.props.history.push({
                                    pathname: `/quiz/result/${res.data.last_id}`
                                });
                            }
                        })
                        .catch(err => {
                            throw new Error('Could not fetch products. Try again later.');
                        });
                }



            }
            else {
                // NEXT QUIZ
                if (answerCheckInArray) {
                    let anserMatchedArray =[];
                    anserMatchedArray=(answerList.filter(answer => (answer.option_name == ans.toUpperCase())))
                    console.log(anserMatchedArray[0].option_number)
                    // console.log(answerList.option_number)
                    // console.log(option_choose)
                    let userAnseredMatched = 'yes';
                    console.log(option_choose_time + 'sub')
                    const { answer_id, points } = selectedQuestion;
                    let option_choose = anserMatchedArray[0].option_number;
                    console.log(answer_id, points, option_choose)
                    this.props.playedQuiz(Object.assign({}, { userAnseredMatched, answer_id, points, option_choose, option_choose_time }));
                    this.props.selectQuestion(selected + 1);
                }
                else {
                    let option_choose = "";
                    let userAnseredMatched = 'no';
                    console.log(option_choose_time + 'sub')
                    const { answer_id, points } = selectedQuestion;
                    console.log(answer_id, points, option_choose)
                    this.props.playedQuiz(Object.assign({}, { userAnseredMatched, answer_id, points, option_choose, option_choose_time }));
                    this.props.selectQuestion(selected + 1);
                }


            }
             this.setState({ answer: '' })

        }



    }

    render() {
        const { selectedQuestion, selected, noOfQuestion } = this.props;
        const { quizSubmitting } = this.state;

        if (selectedQuestion === undefined)
            return (<Loader />);
        let buttonColorClass = ['an1', 'an2', 'an3', 'an4'];
        buttonColorClass = shuffle(buttonColorClass);
        // Variable for options
        let optionsDom;
        const options = JSON.parse(selectedQuestion.options);
        optionsDom = Object.entries(options).map((value, key) => (
            <div key={key} class="">
                <button
                    onClick={() => this.onChooseOption(value[0])}
                    tabindex="0"
                    class={classNames("vix", buttonColorClass[key])}
                    type="button">
                    <span class=""><strong>{value[0]}:&nbsp;</strong>&nbsp;{value[1]}</span>
                    <span class=""></span>
                </button>
            </div>
        ));
        let inputDom;

        inputDom = (<div style={{display:'flex',padding:'28px'}}>
            {console.log(this.state.answer)}
            <MDBInput hint="Write your answer" name="answer"  onChange={e => this.setState({ answer: e.target.value })}/>
           
            <Button bsStyle="info" style={{height:'35px',width:'75px'}}  onClick={() => this.submitAnswer(this.state.answer)}>Submit</Button>
            
        </div>)
        const base_url = `http://demo.quizy.mobi/assets/uploads/logo/`;

        let quiz_image = null;
        if (selectedQuestion.quiz_image)
            quiz_image = (
                <LazyLoadImage
                    effect="blur"
                    height="200px"
                    src={base_url + selectedQuestion.quiz_image}
                />
            );

        return (
            <React.Fragment>
                <DisableBackButton />
                <div class="block pd-15">
                    <div class="col-xs-12 no-padding" style={{ display: 'flex', justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center', marginBottom: '5px' }}>
                        <p style={{ fontSize: '20px', alignSelf: 'flex-end' }}><span style={{ backgroundColor: 'lightgrey', padding: '3px 10px', borderRadius: '25px', color: 'grey', fontStyle: 'italic' }}>{selected} of {noOfQuestion}</span></p>
                        <div style={{ position: 'absolute', width: '100%', textAlign: 'center' }}>
                            <div style={{ color: 'rgba(0, 0, 0, 0.8)', fontSize: '13px', fontStyle: 'italic', fontWeight: '600' }}>{selectedQuestion.points}<br />POINTS</div>
                        </div>
                        <Timer stopTimer={quizSubmitting} start={0} onTimeUp={this.onChooseOption} />
                    </div>
                    <div class="clearfix"></div>
                    <TransitionGroup style={{ position: 'relative' }}>
                        <CSSTransition
                            key={selectedQuestion.quiz_id}
                            timeout={3000}
                            classNames="slide"
                        >
                            <div style={{ position: 'absolute', width: '100%' }}>
                                <div class="quiz-head">
                                    <div class="" style={{ marginTop: '1px', textAlign: 'center' }}>
                                        {quiz_image}
                                    </div>
                                </div>

                                {selectedQuestion.video_url.length > 1 &&
                                    <div>
                                        <IframeComponent src={selectedQuestion.video_url} height="100%" width="100%" />
                                    </div>
                                }
                                {
                                    selectedQuestion.audio_url.length > 1 &&
                                    <div>

                                        <Mp3Component audio={selectedQuestion.audio_url} />
                                    </div>
                                }

                                <div class="quiz-q mt-10">
                                    <div class="col-xs-12 no-padding" style={{ backgroundColor: '#fff', padding: '0px 20px', textAlign: 'justify', borderRadius: '5px', boxShadow: 'rgba(0, 0, 0, 0.3) 0px 2px 5px' }}>
                                        <p>{selectedQuestion.title}</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                {(selectedQuestion.questioninput == '' || selectedQuestion.questioninput == 'no') &&
                                    <div class="quiz-an">
                                        {optionsDom}
                                    </div>
                                }
                                {(selectedQuestion.questioninput == 'yes') &&
                                    <div class="quiz-an">
                                        {inputDom}
                                    </div>
                                }

                            </div>
                        </CSSTransition>
                    </TransitionGroup>
                    <div class="clearfix"></div>
                </div>
                {quizSubmitting && <SubmitLoader msg="while your Quiz is submitted" />}
            </React.Fragment>

        );
    }
};

const mapStateToProps = (state) => {
    console.log(state)
    const { selected, all, played } = state.question;
    const { answer } = state;
    return {
        selectedQuestion: all[selected - 1],
        noOfQuestion: all.length,
        selected: selected,
        playedquiz: played,
        answerList: answer
    }
};

const mapActionsToProps = {
    selectQuestion: selectQuestion,
    playedQuiz: playedQuiz,
};

export default connect(mapStateToProps, mapActionsToProps)(withRouter(PlayingQuiz));