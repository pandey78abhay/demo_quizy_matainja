import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import { Link, withRouter } from 'react-router-dom';
import TemplateWithSlideBar from '../../template/template-with-slide-bar';
import Avatar from 'react-avatar'; 
import { post } from '../../http-api';
import { loggedInUserDetails } from '../../helper/authentication';
import 'react-lazy-load-image-component/src/effects/blur.css';
import './result.css';
import {BASE_URL_PROFILE} from '../../http-api';
import shareIcon from '../../assets/share.png';
import Share from '../../components/share';
import { NoDataFound } from '../no-data-found';

class Result extends Component {
    constructor(props) {
        super(props);
        this.state = {
            result: {},
            noDataFound: false,
            shareComponent: false
        }
    }
    openShareComponent = () => {
      this.setState({shareComponent: true});
      document.getElementById('pageContent').classList.add("overflow-hidden");
    }
    closeShareComponent = () => {
      this.setState({shareComponent: false});
      document.getElementById('pageContent').classList.remove("overflow-hidden");
    }
    componentDidMount() {
        this.getResultById(this.props.match.params.id);
    }
    getResultById = (id) => {
        const payload = new FormData();
        payload.append('result_id', id);
        post('getresult', payload)
        .then(res => {
            if(res.data.success) {
                let { result } = res.data;
                console.log(result);
                this.setState({ result, noDataFound: false });
            }
            if (res.data.error) {
                this.setState({ noDataFound: true });
            }
        })
        .catch(err => {
            this.setState({ noDataFound: true });
            throw new Error('Could not fetch products. Try again later.');
        });
    }
    
    timeFormat = (time) => {
        const min = Math.floor(time / 60);
        const sec = time - min*60;
        return `  ${min}m ${(sec).toFixed(3)}s`;
    }

    render() {
        const { result, shareComponent, noDataFound } = this.state;
        if (noDataFound)
            return(
                <TemplateWithSlideBar>
                    <NoDataFound />
                </TemplateWithSlideBar>
            );
        return(
            <Fragment>
                <TemplateWithSlideBar>
                    <div className="block-title">
                        <h3>Quiz Result</h3>
                    </div>
                    <div class="share-result"> 
                        <div className="share-icon-result" onClick={()=>this.openShareComponent()}>
                            <span>Share</span>
                            <img src={shareIcon} />
                        </div>
                    </div>
                    <div className="block pd-15">
                        <div className="quiz-info">
                            <h5>
                                Quiz: <span style={{textTransform: 'capitalize'}}>{result.category_title}</span>
                                <span className="pull-right"><i className="fa fa-clock"></i> {this.timeFormat(result.total_time)}</span>
                            </h5>
                        </div>
                        <div className="set"> 
                            <div className="set-img">
                                {result && <Avatar
                                    round={true}         
                                    size="140" 
                                    src={result.image ? `${BASE_URL_PROFILE+result.image}` : null}
                                    name={'Goutam Das'}
                                />}
                            </div>
                            <div className="set-bar">
                                <div className="set-extra">
                                    <h5 className="desc1">Right Answer: <span>{result.right}</span></h5>
                                </div>
                                <div className="set-extra">
                                    <h5 className="desc2">Wrong Answer: <span>{result.wrong}</span></h5>
                                </div>
                                <div className="set-extra">
                                    <h5 className="desc1">Accurate: <span>{(parseFloat(result.accurate)).toFixed(2)}%</span></h5>
                                </div> 
                            </div>
                        </div>
                        <Score score={result.score} bestScore={result.best_score} />
                        </div>
                        <div className="clearfix"></div>
                        <div className="bottom-fixed-status">
                            <ul className="list-inline">
                                <li className="bottom-button">
                                    <a onClick={() => this.props.history.goBack()}>
                                        <div><i class="fas fa-undo-alt"></i></div>Attempt Again
                                    </a>
                                </li>
                                <li className="bottom-button">
                                    <Link to={{pathname: '/'}}>
                                        <div><i class="fas fa-home"></i></div>Home
                                    </Link>
                                </li>
                                <li className="bottom-button">
                                    <Link to={`/leaderboard/${result.category_id}`}>
                                        <div><i class="fas fa-trophy"></i></div>Leader Board
                                    </Link>
                                </li>
                            </ul>
                        </div>
                </TemplateWithSlideBar>
                {shareComponent && <Share closeShareComponent={this.closeShareComponent}/>}
            </Fragment>
        );
    }
};
const mapStateToProps = (state) => ({
  playedQuestions: state.question.played,
  answer: state.answer  
});

const mapActionsToProps = {
    
};

export default connect(mapStateToProps, mapActionsToProps)(withRouter(Result));

const Score = ({score, bestScore}) => (
    <div className="score-best-score">
        <div>
            <div>
                <div>Score<span>{score}</span></div>
                <div>Best Score<span>{bestScore}</span></div>
            </div>
        </div>
    </div>
);