import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import './details.css';
import shareIcon from '../../../assets/share.png';
import Share from '../../../components/share';
import { BASE_URL_IMAGE } from '../../../http-api';
import { DetailsSimmer, TitleSimmer, DetailsBannerSimmer } from '../../../content-loader';
import { NoDataFound } from '../../no-data-found';
import Leaderboard from '../../leaderboard/Leaderboard';

class Details extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scrollFixed: false,
            shareComponent: false
        }
    }
    // openShareComponent = () => {
    //   this.setState({shareComponent: true});
    //   document.getElementById('pageContent').classList.add("overflow-hidden");
    // }
    // closeShareComponent = () => {
    //   this.setState({shareComponent: false});
    //   document.getElementById('pageContent').classList.remove("overflow-hidden");
    // }
    // componentDidMount() {
    //     document.getElementById('pageContent').addEventListener('scroll', this.handleScroll);
    // }

    // componentWillUnmount() {
    //     document.getElementById('pageContent').removeEventListener('scroll', this.handleScroll);
    // }

    handleScroll = event => {
      if (event.srcElement.scrollTop >= 206)
        this.setState({
          scrollFixed: true
        });
      else
        this.setState({
          scrollFixed: false
        });
    }
    render() {
        const { scrollFixed, shareComponent } = this.state;
        const {children, categoryDetails, noDataFound} = this.props;
        
        if (noDataFound)
            return(<NoDataFound />);

        return(
            <Fragment>
              <div class="col-xs-12 pd-10">  
                <div className="rewards-card">
                  <div className="fixed-off">
                    <div className="fixed-off1">
                    <div className="rewards-img">
                            {categoryDetails.title_image && <img
                                height="150px"
                                src={BASE_URL_IMAGE+categoryDetails.title_image}
                                style={{width: '100%', objectFit: 'cover', objectPosition: 'top center'}}
                                className="image-responsive"
                            />}
                            {!categoryDetails.title_image && <DetailsBannerSimmer />}
                            <div className="coin"><strong>{categoryDetails.total} {categoryDetails.total<2 ? 'Question' : 'Questions'}</strong></div>
                            <div className="share-icon">
                                {Object.keys(categoryDetails).length ? <img onClick={()=>this.openShareComponent()} src={shareIcon} />:<img src={shareIcon} />}
                            </div>
                        </div>
                        <div className="pd-10">
                            <div class="col-xs-9 pd-0">
                                <h4 class="mt-0" style={{textTransform: 'capitalize'}}>{categoryDetails.title? categoryDetails.title : <TitleSimmer />}</h4>
                                <div class="info-game-rating">
                                    <span class="info-game-rating">
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                    </span>
                                </div> 
                            </div>
                            <div className="col-xs-3 pd-0">
                                {Object.keys(categoryDetails).length ? children :  
                                <button disabled
                                    class="pure-material-button-contained"
                                    style={{textTransform: 'capitalize', top: '15px', letterSpacing: '0.5px'}}
                                    ><span style={{paddingRight: '5px'}}>&#9658;</span>Play</button>}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        </div>
                        </div>
                        <div class="col-xs-12 pd-10" style={{marginTop: '230px'}}>
                            { !categoryDetails.description && <DetailsSimmer />}
                            { categoryDetails.description && <div className="text-desc" style={{lineHeight: '20px', letterSpacing: '0.5px'}} dangerouslySetInnerHTML={{ __html: categoryDetails.description }}/>}
                        </div>
                    </div>					    		
                </div>
                <Leaderboard/>
                {shareComponent && <Share closeShareComponent={this.closeShareComponent}/>}
            </Fragment>
        );
    }
};

export default withRouter(Details);

