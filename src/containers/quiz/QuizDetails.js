import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import QuizDetailsTemplate from '../../template/template-with-goback-button';
import DetailsComponent from './components/details';
import SubmitLoader from './submit-loader'; 

import { fetchQuestionByCatId } from '../../store/actions';
import { resetPlayedQuiz, selectQuestion,fetchAnswerByCatId } from '../../store/actions'; 

import { isLoggedIn } from '../../helper/authentication';
import { post, post_loggedInUser } from '../../http-api';

import Swal from 'sweetalert2';
import {BASE_URL_IMAGE} from '../../http-api';
class QuizDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quizPreparing: false,
            categoryDetails: {},
            noDataFound: false
        }
    }

    componentDidMount() {
        this.getDetailsById(this.props.match.params.id);
    }
    
    playQuiz = (quiz) => {
        if(isLoggedIn()){ 
            // Authenticated user
            const { categoryDetails } = this.state;
            let error = false;
            const payload = new FormData();
            payload.append("category_id", this.props.match.params.id);

            post_loggedInUser('noOfAttemptquiz',payload)
            .then(res => {
                // if(true) {
                    // Swal({
                    //     title: quiz.title,
                    //     html: `<h3 style="text-align: center;margin-top: 0px;margin-bottom: 10px;text-transform: uppercase;">Instructions</h3>
                    //     <ul>
                    //         <li style="text-align: left; font-size: 15px; list-style-type: none; line-height: 25px;"><i class="fa fa-hand-o-right"></i>&nbsp;No. of Questions: ${categoryDetails.total}</li>
                    //         <li style="text-align: left; font-size: 15px; list-style-type: none; line-height: 25px;"><i class="fa fa-hand-o-right"></i>&nbsp;DO NOT Refresh the page!</li>
                    //         <li style="text-align: left; font-size: 15px; list-style-type: none; line-height: 25px;"><i class="fa fa-hand-o-right"></i>&nbsp;All The Best</li>
                    //     </ul>`,
                    //     imageUrl: BASE_URL_IMAGE+categoryDetails.title_image,
                    //     confirmButtonText: "START",
                    //     imageClass: 'img-responsive rounded-circle',
                    // }).then((result) => {
                        if (!res.data.error) {
                            this.setState({ quizPreparing: true });
                            const payload = new FormData();
                            payload.append("category_id", this.props.match.params.id);
                            post_loggedInUser('getquestion', payload)
                                .then(res => {
                                    this.setState({
                                        quizPreparing: false
                                    });
                                    this.props.fetchQuestionByCatId([]);
                                    if (res.data.success) {
                                        let { questionlist } = res.data;
                                        this.props.fetchQuestionByCatId(questionlist);
                                        this.props.fetchAnswerByCatId(res.data.answerlist)
                                        this.props.playQuiz();
                                    }
                                })
                                .catch(err => {
                                    throw new Error('Could not fetch products. Try again later.');
                                });
                        }
                    // });
                // }

                if(res.data.error) {
                    Swal({
                        position: 'center',
                        type: 'error',
                        title: 'Quiz Attempt Limit is over!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            })
            .catch(err => {
                throw new Error('Could not fetch products. Try again later.');
            });
            this.props.resetPlayedQuiz();
            this.props.selectQuestion(1);

            // const {categoryDetails} = this.state;
            
            
            
        } else {
            // Not Authenticated user
            this.props.history.push({
                pathname: '/login',
                state: {from: '/quiz/details', quiz}
            });
        }
    }
    getDetailsById = (id) => {
        const payload = new FormData();
        payload.append('category_id', id);
        post('detailsbycatid', payload)
        .then(res => {
            if(res.data.success) {
                let { category_details } = res.data;
                this.setState({categoryDetails: category_details, noDataFound: false});
            }
            if (res.data.error) {
                this.setState({noDataFound: true});
            }
        })
        .catch(err => {
            throw new Error('Could not fetch products. Try again later.');
        });
    }
    render() {
        const { categoryDetails, quizPreparing, noDataFound } = this.state;

        return(
            <Fragment>
                <QuizDetailsTemplate>
                    <DetailsComponent noDataFound={noDataFound} categoryDetails={categoryDetails}>
                        <button
                            onClick={()=>this.playQuiz(this.props.match.params.id)}
                            class="pure-material-button-contained"
                            style={{textTransform: 'capitalize', top: '15px', letterSpacing: '0.5px'}}
                        ><span style={{paddingRight: '5px'}}>&#9658;</span>Play</button>
                    </DetailsComponent>

                </QuizDetailsTemplate >
                {quizPreparing && <SubmitLoader msg="while your Quiz is prepared"/>}
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => ({});

const mapActionsToProps = {
    fetchQuestionByCatId: fetchQuestionByCatId,
    resetPlayedQuiz: resetPlayedQuiz,
    selectQuestion: selectQuestion,
    fetchAnswerByCatId:fetchAnswerByCatId
};

export default connect(mapStateToProps, mapActionsToProps)(withRouter(QuizDetails));

