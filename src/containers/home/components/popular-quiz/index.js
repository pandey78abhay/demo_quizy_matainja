import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import Swiper from 'react-id-swiper';
import 'swiper/dist/css/swiper.min.css';
import { PopularQuizLoader } from '../../../../content-loader';
import { fetchPopularQuiz } from '../../../../store/actions';
import { post } from '../../../../http-api';
import './index.css';
import {BASE_URL_IMAGE} from '../../../../http-api';
const params = {
    slidesPerView: 2,
    slidesPerColumn: 2,
    spaceBetween: 10,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    }
};

class PopularQuiz extends Component {
    constructor(props) {
        super(props);
        this.state = {loading: true} 
    }

    componentDidMount() {
        post('popularquiz')
        .then(res=>{
            if(res.data.success) {
                const {popularquiz} = res.data;
                this.props.fetchPopularQuiz(popularquiz);
                this.setState({loading: false});
            }
        })
        .catch(err => {
            this.setState({loading: false});
            throw new Error('Could not fetch products. Try again later.');
        });
    }

    render() {  
        const { popularQuiz } = this.props;
        if (popularQuiz.length)
            return(
                <React.Fragment>
                    <div className="block pd-5 m-5" style={{margin: '5px'}}>
                        <Swiper {...params}>
                            {popularQuiz && popularQuiz.map((data, key) => (
                                <div key={key} className="thumb">
                                    <Link to={`/quiz/details/${data.id}`} 
                                        style={{position: 'relative', display: 'block'}}
                                    >
                                        <img
                                            className="img-responsive popular-quiz-img"
                                            src={BASE_URL_IMAGE+data.title_image}
                                        />
                                        <div className="title" style={{whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', maxWidth: '100%'}}>{data.title}</div>
                                        <div className="no-of-question">{data.total} Questions</div>
                                    </Link>
                                </div>
                                ))
                            }
                        </Swiper>
                        <div className="clearfix"></div>
                    </div>
                </React.Fragment>
            );
        return(<PopularQuizLoader />);
    }
};

const mapStateToProps = (state) =>({
    popularQuiz: state.quiz.popular,
});

const mapActionsToProps = {
    fetchPopularQuiz: fetchPopularQuiz,
};

export default connect(mapStateToProps, mapActionsToProps)(PopularQuiz);