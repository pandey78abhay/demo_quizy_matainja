import React from 'react';
import {Link} from 'react-router-dom';
const base_url = `http://demo.quizy.mobi/assets/uploads/logo/`;

const PopularQuizItem = ({data}) => (
    <div className="thumb">
        <Link to={'/quiz/details'} 
                style={{position: 'relative', display: 'block'}}
        >
            <img
                class="img-responsive"
                src={base_url+data.title_image}
                style={{height: '150px', borderRadius: '10px 10px 0px 0px', objectFit: 'cover', objectPosition: 'center'}}
            />
            <div className="title" style={{whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', maxWidth: '100%'}}>{data.title}</div>
            <div className="desc" 
            style={{position: 'absolute', top: 0, backgroundColor: 'rgba(0, 0, 0, 0.72)', width: '100%', color: 'white', borderRadius: '10px 10px 0px 0px'}}>{data.total} Questions</div>
        </Link>
    </div>
);

export default PopularQuizItem;