import React, { Component } from 'react';
import { post } from '../../http-api';
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
// import QuizPlayTemplate from '../quiz/components/QuizDetailsTemplate'
import QuizDetailsComponent from '../../containers/quiz/QuizDetails';
import MatchedPlayers from './MatchedPlayers';
import { fetchPVPQuestionByCatId } from '../../store/actions';
import { resetPVPPlayedQuiz, selectPVPQuestion, fetchPVPAnswerByCatId } from '../../store/actions';
import PVPForQuestions from '../PVPForQuestons';

class pairForDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            quizScreen: false
        }


    }
    componentDidMount() {
       
        setTimeout(() => {
            
            this.setState({ quizScreen: true })
        }, 10000);

    }
    render() {
        const { pairDetails } = this.state;
        const pair_ids = [
            {
                'user_id': this.props.pair_details.pair_creater.user_id,

            },
            {
                'user_id': this.props.pair_details.pair_accepter.user_id
            }
        ]
        console.log(pair_ids)
        console.log(this.props.pair_details)
        console.log(this.props)
        return (
            <div>
                {!this.state.quizScreen &&
                    <MatchedPlayers pairDetails={this.props.pair_details} />
                }
                {this.state.quizScreen &&
                    <PVPForQuestions userId={this.props.userId} categoryId={this.props.categoryId} pair_ids={pair_ids} pair_id={this.props.pair_details.pair_id} />
                }
            </div>


        )
    }
}
const mapStateToProps = (state) => ({});

const mapActionsToProps = {
    fetchPVPQuestionByCatId: fetchPVPQuestionByCatId,
    resetPVPPlayedQuiz: resetPVPPlayedQuiz,
    selectPVPQuestion: selectPVPQuestion,
    fetchPVPAnswerByCatId: fetchPVPAnswerByCatId
};

export default connect(mapStateToProps, mapActionsToProps)(withRouter(pairForDetails));