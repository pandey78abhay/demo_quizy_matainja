import React from 'react';
import PVPImgage from '../../../assets/img/pvp.png';
import TemplateWithGoBackButton from '../../template/template-with-goback-button';
import { BASE_URL_PROFILE } from '../../http-api';
const MatchedPlayers = (props) => {
    return (
        <TemplateWithGoBackButton>

            <div>
                {console.log(props.pairDetails.pair_creater.image)}
                <h2 style={{ textAlign: 'center', paddingTop: '20px' }}>Oppenent Found</h2>
                <div className="container'" style={{ padding: '50px' }}>
                    <div style={{ display: 'flex', float: 'left' }}>
                        <img style={{ width: '50px', borderRadius: '50%' }} src={BASE_URL_PROFILE + props.pairDetails.pair_creater.image} />
                        <h2 style={{ background: '#6877de', borderRadius: '7px', paddingLeft: '10px', paddingRight: '10px', fontWeight: 'bold' }}>{props.pairDetails.pair_creater.first_name}</h2>
                    </div>
                    <div style={{ width: '200px', height: '200px', paddingTop: '100px', paddingBottom: '200px' }} className="container">
                        <img src={PVPImgage} />
                    </div>
                    <div style={{ display: 'flex', float: 'right' }}>

                        <h2 style={{ background: '#6877de', borderRadius: '7px', paddingLeft: '10px', paddingRight: '10px', fontWeight: 'bold' }}>{props.pairDetails.pair_accepter.first_name}</h2>
                        <img style={{ width: '50px', borderRadius: '50%' }} src={BASE_URL_PROFILE + props.pairDetails.pair_accepter.image} />
                    </div>

                </div>
                <h2 style={{ textAlign: 'center', paddingTop: '20px', color: 'green' }}>Match Starts in 5 Sconds...</h2>
            </div>

        </TemplateWithGoBackButton>

    )
}
export default MatchedPlayers;