import React, { Component, Fragment } from 'react';
import { Link, withRouter } from 'react-router-dom';

import TemplateWithSlideBar from '../../template/template-with-slide-bar';
import { post } from '../../http-api';
import './leader-board-all.css';
import ScoreItem from './component/scoreItem';
import {LeaderBoardSimmer, AllLeaderBoardSimmer} from '../../content-loader';
const LeaderBoard = ({scores}) => (
    <div class="leaderboard">
        <div class="row margin-bottom-5 pd-right-5 pd-left-5">
            <div class="col-xs-2 text-center">Rank</div>
            <div class="col-xs-6">Player</div>
            <div class="col-xs-4 text-right">Best Score</div>
        </div>
        <div class="table-responsive">          
            <table class="table v2">
                <tbody>
                    {scores.length && scores.map((score, key) =><ScoreItem key={key} rank={key+1} score={score} />)}
                </tbody>
            </table>
        </div>
    </div>
);

const LeaderBoardHistoryByCategory = ({category_id, title, scores}) => (
    <Fragment>
        <div class="block pd-0">
            <div class="lead-info mb-0" style={{width: '100%', color: '#FF4E1A'}}>
                <h5 style={{fontSize: '18px', marginBottom: '0px', marginLeft: '15px'}}>Quiz: {title} 
                    <span class="seemore" style={{float: 'right', textAlign: 'right', fontSize: '12px', color: '#0288d1 !important', marginRight: '18px', marginTop: '5px'}}>
                        <Link to={`/leaderboard/${category_id}`}>View</Link>
                    </span>
                </h5>
            </div>
        </div>
        <div class="block pd-15 pd-top-6">
            <LeaderBoard scores={scores} />
        </div>
    </Fragment>
);

class LeaderBoardAll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scores: [],
            hasMore: true,
            page: -1
        } 
    }
    componentDidMount() {
        this.getScore();
    }
    getScore = (page=0) => {
        const payload = new FormData();
        payload.append('page',page);
        post('getscorenew', payload)
        .then(res => {
            const { score_details, hasmore } = res.data;
            this.setState((prevState)=>({
                scores: [...prevState.scores,...score_details],
                hasMore: hasmore,
                page: prevState.page + 1
            }));
        })
        .catch(err => {
            console.log(err);
            throw new Error('Could not fetch products. Try again later.');
        });
    }
    nextPage = () => {
        const { page, hasMore } = this.state;
        if(hasMore){
            this.getScore(page+ 1);
        }
    }
    prevPage = () => {
        const { page } = this.state;
        if(!!page)
            this.setState((prevState)=>({page: prevState.page - 1, hasMore: true}));
    }
    render() {
        const { scores, hasMore, page } = this.state;
        const limit = 3;
        let start = page*limit;
        let end = limit*(page+1);
        return( 
            <React.Fragment>
                <TemplateWithSlideBar>
		            <div class="block-title block-position-fixed"><h3>All Leaderboard</h3></div>
                    <div className="pd-15 mt-20"></div>
                    {!scores.length && <AllLeaderBoardSimmer />}
                    {scores && scores.slice(start, end).map((score, key)=><LeaderBoardHistoryByCategory 
                        category_id={score.category_id}
                        title={score.title}
                        scores={score.scores}
                    />)}
                    {!!scores.length && <ul class="pager" style={{padding: '0px 15px'}}>
                        {!!page && <li class="previous"><a onClick={this.prevPage}>Previous</a></li>}
                        {hasMore && <li class="next"><a onClick={this.nextPage}>Next</a></li>}
                    </ul>}
                </TemplateWithSlideBar>
            </React.Fragment>
        );
    }
};

export default withRouter(LeaderBoardAll);

