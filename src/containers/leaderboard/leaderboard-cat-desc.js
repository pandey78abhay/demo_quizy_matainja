import React, { Component, Fragment } from 'react';
import { Link, withRouter } from 'react-router-dom';

import TemplateWithSlideBar from '../../template/template-with-slide-bar';
import { post } from '../../http-api';
import './leader-board-all.css';
import ScoreItem from './component/scoreItem';
import {LeaderBoardSimmer, AllLeaderBoardSimmer} from '../../content-loader';
import imgRank from '../../assets/rankimg.png';
import {BASE_URL_PROFILE} from '../../http-api';
const LeaderBoard = ({scores}) => (
    <div class="leaderboard">
        <div class="row margin-bottom-5 pd-right-5 pd-left-5">
            <div class="col-xs-2 text-center">Rank</div>
            <div class="col-xs-6">Player</div>
            <div class="col-xs-4 text-right">Best Score</div>
        </div>
        <div class="table-responsive">          
            <table class="table v2">
                <tbody>
                    {scores.length && scores.map((score, key) =><ScoreItem key={key} rank={key+1} score={score} />)}
                </tbody>
            </table>
        </div>
    </div>
);

const LeaderBoardHistoryByCategory = ({category_id, title, scores}) => (
    <Fragment>
        <div class="block pd-0">
            <div class="lead-info mb-0" style={{width: '100%', color: '#FF4E1A'}}>
                <h5 style={{fontSize: '18px', marginBottom: '0px', marginLeft: '15px'}}>Quiz: {title} 
                    <span class="seemore" style={{float: 'right', textAlign: 'right', fontSize: '12px', color: '#0288d1 !important', marginRight: '18px', marginTop: '5px'}}>
                        <Link to={`/leaderboard/${category_id}`}>View</Link>
                    </span>
                </h5>
            </div>
        </div>
        <div class="block pd-15 pd-top-6">
            <LeaderBoard scores={scores} />
        </div>
    </Fragment>
);

class LeaderBoardAll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scores: [],
            hasMore: true,
            page: -1
        } 
    }
    componentDidMount() {
        this.getScore();
    }
    getScore = (page=0) => {
        post('getleaderboard')
        .then(res => {
            if (res.data.success === 1) {
                const { score_list } = res.data;
                this.setState({scores: score_list});
            }
        })
        .catch(err => {
            console.log(err);
            throw new Error('Could not fetch products. Try again later.');
        });
    }
    nextPage = () => {
        const { page, hasMore } = this.state;
        if(hasMore){
            this.getScore(page+ 1);
        }
    }
    prevPage = () => {
        const { page } = this.state;
        if(!!page)
            this.setState((prevState)=>({page: prevState.page - 1, hasMore: true}));
    }
    render() {
        const { scores, hasMore, page } = this.state;
        const limit = 3;
        let start = page*limit;
        let end = limit*(page+1);
        console.table(scores);
        const scoreBarWidth = ['90%', '70%', '50%'];
        return( 
            <React.Fragment>
                <TemplateWithSlideBar>
		            {/* <div class="block-title block-position-fixed"><h3>Leaderboard ALT</h3></div> */}
                    <div className="pd-15 mt-20">
                        <div class="leaderboard">
				            <div class="table-responsive">          
					            <table class="table v2">
                                    <tbody>
                                        {scores && scores.map((score, key)=>
                                            <ScoreDetailsComponent 
                                                key={key}
                                                imgUrl={BASE_URL_PROFILE+score.image}
                                                rank={key+1}
                                                name={score.first_name+' '+score.last_name}
                                                scoreBarWidth={scoreBarWidth[key]}
                                                score={score.score}
                                                time={timestampFormat(score.total_time)}
                                                coin={score.coin}
                                            />
                                        )}
						        </tbody>
					        </table>
				        </div>
			        </div>
                    </div>
                    {/* {!scores.length && <AllLeaderBoardSimmer />} */}
                    {/* {scores && scores.map((score, key)=><ScoreDetailsComponent
                        key={key}
                        rank={key + 1}
                        name={score.name}
                        image={score.image}
                        time={score.total_time}
                        title={score.cat_title}
                        score={score.score}
                        rightQuestion={score.right}
                        totalQuestion={score.total_question}
                    />)} */}
                </TemplateWithSlideBar>
            </React.Fragment>
        );
    }
};

export default withRouter(LeaderBoardAll);


const ScoreDetailsComponent = ({imgUrl, rank, name, scoreBarWidth, score, time, coin}) => (
    <tr>
        <td width="10%">
            <div class="set-img">
                <img src={imgUrl} class="img-circle" alt="" /> 
            </div>
        </td>
        <td width="80%">
            <div class="top">
                <span class="text-orange">{rank}.</span> {name}
            </div> 
            <div class="mid">
                <hr width={scoreBarWidth} />
            </div>
            <div class="botm">
                <p class="badge text-small clr-orange">Score</p>
                <p class="mt-3">{score}</p>
                <p class="badge text-small clr-orange">Time</p>
                <p class="mt-3">{time}</p>
            </div>
        </td>
        <td class="text-coin">{coin}<br />Coin</td>
    </tr>
);

const timestampFormat = (time) => {
    time=time*1000;
    const min = Math.floor(time/(60*1000));
    time = (time - min*(60*1000));
    const sec = Math.floor(time/1000);
    const milisec = time - sec * 1000;
    return ` ${min>9? min: '0'+min}:${sec>9? sec: '0'+sec}:${milisec>9? Math.floor(milisec/10): '0'+milisec}`;
}