import React, { Component } from 'react';
import classNames from 'classnames';
import { withRouter } from 'react-router-dom';
import { FormGroup, FormControl } from 'react-bootstrap';
import TemplateWithGoBackButton from '../../template/template-with-goback-button';
import './leader-board.css';
import { post_loggedInUser } from '../../http-api';
import ScoreItem from './component/score-item';

class LeaderBoard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'all_time',
            scores: [],
            categoryTitle: '',
            scrollFixed: false,
            noData: false
        }
    }

    componentDidMount() {
        document.getElementById('pageContent').addEventListener('scroll', this.handleScroll);
        this.getScore(this.state.type);
    }

    componentWillUnmount() {
        document.getElementById('pageContent').removeEventListener('scroll', this.handleScroll);
    }

    onSelectType = (e) => {
        this.setState({type: e.target.value});
        this.getScore(e.target.value);
    }

    handleScroll = event => {
        if (event.srcElement.scrollTop >= 18)
            this.setState({
                scrollFixed: true
            });
        else
            this.setState({
                scrollFixed: false
            });
        // console.log('scroll');
         console.log(event.srcElement.scrollTop);
    }
    getScore = (type) => {
        const { match } = this.props;
        var form = new FormData();
        form.append("category_id", match.params.id);
        form.append("type", type);
            post_loggedInUser('getscorebycategory', form)
            .then(res => {
                if (res.data.success) {
                    const {score_details, category_title} = res.data;
                    this.setState({
                        scores: score_details,
                        categoryTitle: category_title,
                        noData: false
                    });
                }
                if (res.data.error) {
                    this.setState({
                        scores: [],
                        noData: true
                    });
                }
            })
            .catch(err => {
                console.log(err);
                throw new Error('Could not fetch products. Try again later.');
            });
    }
    render() {
        const { type, scores, categoryTitle, scrollFixed, noData } = this.state;
        return(
            <React.Fragment>
                <TemplateWithGoBackButton>
                <div className={classNames({"block-position-fixed": scrollFixed})}>
                    <div className={classNames("block-title",{"block-position-fixed":!scrollFixed})}>
                        <h3>Leaderboard</h3>
                    </div>
                    <div style={{marginTop: '40px'}} className={classNames("row", "pd-right-15", "pd-left-15", {"display-none": !scrollFixed}, {"block-box-shadow":scrollFixed})}>
                        <div className="col-xs-2">Rank</div>
                        <div className="col-xs-6">Player</div>
                        <div className="col-xs-4 text-center">Score</div>
                    </div>
                </div>
                <div className="pd-15 mt-20"></div>
                <div className="block pd-15">
                    <div className={classNames("quiz-info", "bg-brown", "mb-0", {"select-box-fixed":scrollFixed})}>
                        <h5>Quiz: <span style={{textTransform: 'capitalize'}}>{categoryTitle}</span>
                            <span className="pull-right">
                                <FormGroup controlId="formControlsSelect">
                                    <FormControl 
                                        componentClass="select"
                                        className="fix-form" 
                                        placeholder="select"
                                        value={type}
                                        onChange={this.onSelectType}
                                    >
                                        <option value="this_day">This Day</option>
                                        <option value="this_week">This Week</option>
                                        <option value="this_month">This Month</option>
                                        <option value="all_time">All Time</option>
                                    </FormControl>
                                </FormGroup>
                            </span>
                        </h5>
                    </div>
                </div>
                <div className="block pd-15">
			        <div className="leaderboard">
				        <div className="row">
					        <div className="col-xs-2">Rank</div>
					        <div className="col-xs-6">Player</div>
					        <div className="col-xs-4 text-center">Best Score</div>
				        </div>
				        <div className="table-responsive">          
					        <table className="table v2">
                                <tbody>
                                    {scores && scores.map((score, key)=>(<ScoreItem key={key} rank={key+1} score={score}/>))}
                                </tbody>
					        </table>
                            {noData && <div className="no-data-found-on-leader-board">No Data Found!</div>}
				        </div>
			        </div>
		        </div>
                </TemplateWithGoBackButton>
            </React.Fragment>
        );
    }
};

export default withRouter(LeaderBoard);