import React from 'react';
import Avatar from 'react-avatar';
import './score-item.css'; 
import {BASE_URL_PROFILE} from '../../../http-api';

const ScoreItem = ({rank, score}) => (
    <tr>
        <td class="text-center">{rank}</td>
        <td style={{position: 'relative'}}>
            <div class="set-img">
                <Avatar
                    round={true} 
                    size="46" 
                    src={score.image ? `${BASE_URL_PROFILE}${score.image}` : null} 
                    name={score.name}
                />
            </div>
            {score.name}
        </td>
        <td class="text-right">{score.score}</td>
    </tr>
);

export default ScoreItem;

const timestampFormat = (time) => {
    const min = Math.floor(time/(60*1000));
    time = (time - min*(60*1000));
    const sec = Math.floor(time/1000);
    const milisec = time - sec * 1000;
    return ` ${min>9? min: '0'+min}:${sec>9? sec: '0'+sec}:${milisec>9? milisec: '0'+milisec}`;
}