import React, { Component, Fragment } from 'react';
import { Link, withRouter } from 'react-router-dom';

import TemplateWithSlideBar from '../../template/template-with-slide-bar';
import { post } from '../../http-api';
import './leader-board-all.css';
import ScoreItem from './component/scoreItem';
import {LeaderBoardSimmer, AllLeaderBoardSimmer} from '../../content-loader';
import imgRank from '../../assets/rankimg.png';
import {BASE_URL_PROFILE} from '../../http-api';
const LeaderBoard = ({scores}) => (
    <div class="leaderboard">
        <div class="row margin-bottom-5 pd-right-5 pd-left-5">
            <div class="col-xs-2 text-center">Rank</div>
            <div class="col-xs-6">Player</div>
            <div class="col-xs-4 text-right">Best Score</div>
        </div>
        <div class="table-responsive">          
            <table class="table v2">
                <tbody>
                    {scores.length && scores.map((score, key) =><ScoreItem key={key} rank={key+1} score={score} />)}
                </tbody>
            </table>
        </div>
    </div>
);

const LeaderBoardHistoryByCategory = ({category_id, title, scores}) => (
    <Fragment>
        <div class="block pd-0">
            <div class="lead-info mb-0" style={{width: '100%', color: '#FF4E1A'}}>
                <h5 style={{fontSize: '18px', marginBottom: '0px', marginLeft: '15px'}}>Quiz: {title} 
                    <span class="seemore" style={{float: 'right', textAlign: 'right', fontSize: '12px', color: '#0288d1 !important', marginRight: '18px', marginTop: '5px'}}>
                        <Link to={`/leaderboard/${category_id}`}>View</Link>
                    </span>
                </h5>
            </div>
        </div>
        <div class="block pd-15 pd-top-6">
            <LeaderBoard scores={scores} />
        </div>
    </Fragment>
);

class LeaderBoardAll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scores: [],
            hasMore: true,
            page: -1
        } 
    }
    componentDidMount() {
        this.getScore();
    }
    getScore = (page=0) => {
        const payload = new FormData();
        payload.append('page',page);
        post('getscorenew1')
        .then(res => {
            console.log(res);
            const { score_details } = res.data;
            this.setState((prevState)=>({
                scores: score_details,
            }));
        })
        .catch(err => {
            console.log(err);
            throw new Error('Could not fetch products. Try again later.');
        });
    }
    nextPage = () => {
        const { page, hasMore } = this.state;
        if(hasMore){
            this.getScore(page+ 1);
        }
    }
    prevPage = () => {
        const { page } = this.state;
        if(!!page)
            this.setState((prevState)=>({page: prevState.page - 1, hasMore: true}));
    }
    render() {
        const { scores, hasMore, page } = this.state;
        const limit = 3;
        let start = page*limit;
        let end = limit*(page+1);
        console.table(scores);
        return( 
            <React.Fragment>
                <TemplateWithSlideBar>
		            <div class="block-title block-position-fixed"><h3>All Leaderboard</h3></div>
                    <div className="pd-15 mt-20"></div>
                    {!scores.length && <AllLeaderBoardSimmer />}
                    {scores && scores.map((score, key)=><ScoreDetailsComponent
                        key={key}
                        rank={key + 1}
                        name={score.name}
                        image={score.image}
                        time={score.total_time}
                        title={score.cat_title}
                        score={score.score}
                        rightQuestion={score.right}
                        totalQuestion={score.total_question}
                    />)}
                </TemplateWithSlideBar>
            </React.Fragment>
        );
    }
};

export default withRouter(LeaderBoardAll);


const ScoreDetailsComponent = ({rank, name, image, title, score, time, rightQuestion, totalQuestion}) => (
    <div className="container" style={{margin: '2px 5px'}}>
		<div className="row score-details">
			<div className="col-xs-3 rank-info">
				<div>
					<img src={imgRank} alt="Avatar" className="rank" />
					<div><div>{rank}</div></div>
				</div>
				<div><div>Rank</div></div>
			</div>
			<div className="col-xs-9">
				<div className="row">
					<div className="col-xs-12 cat-info">
						<div>{title}</div>
					</div>
				</div>
				<div className="row" style={{marginBottom: '10px', marginTop: '6px'}}>
					<div className="col-xs-3">
						<img src={BASE_URL_PROFILE+image} alt="Avatar" className="avatar" />
					</div>
					<div class="col-xs-9" style={{paddingLeft: '0px'}}>
						<div style={{marginTop: '10px', textTransform: 'capitalize'}}>{name}</div>
					</div>
				</div>
				<div className="row">
					<div className="col-xs-4 score-info">
						<div><div>{score}</div></div>
						<div><div>Score</div></div>
					</div>
					<div className="col-xs-4 time-info">
						<div><div>{timestampFormat(time)}</div></div>
						<div><div>Time</div></div>
					</div>
					<div className="col-xs-4 ques-info">
						<div><div>{rightQuestion} / {totalQuestion}</div></div>
						<div><div>Attempt</div></div>
					</div>
				</div>
			</div>
		</div>
	</div>
);

const timestampFormat = (time) => {
    time=time*1000;
    const min = Math.floor(time/(60*1000));
    time = (time - min*(60*1000));
    const sec = Math.floor(time/1000);
    const milisec = time - sec * 1000;
    return ` ${min>9? min: '0'+min}:${sec>9? sec: '0'+sec}:${milisec>9? Math.floor(milisec/10): '0'+milisec}`;
}