import React, { Component } from 'react';
import Swiper from 'react-id-swiper';
import 'swiper/dist/css/swiper.min.css';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import TemplateWithGoBackButton from '../../template/header-with-back-button';
import { post } from '../../http-api';
import { fetchPopularQuiz } from '../../store/actions';
import { BASE_URL_IMAGE } from '../../http-api';
import './index.css';
const params = {
    slidesPerView: 2,
    slidesPerColumn: 2,
    spaceBetween: 10,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    }
};
// const data = [
//     {
//         id: 0,
//         img: 'https://images-na.ssl-images-amazon.com/images/I/412aXaVP%2BAL.jpg',
//         ques: 10,
//         title: 'sports'
//     },
//     {
//         id: 0,
//         img: 'https://images-na.ssl-images-amazon.com/images/I/412aXaVP%2BAL.jpg',
//         ques: 10,
//         title: 'sports'
//     },
//     {
//         id: 0,
//         img: 'https://images-na.ssl-images-amazon.com/images/I/412aXaVP%2BAL.jpg',
//         ques: 10,
//         title: 'sports'
//     },
//     {
//         id: 0,
//         img: 'https://images-na.ssl-images-amazon.com/images/I/412aXaVP%2BAL.jpg',
//         ques: 10,
//         title: 'sports'
//     },
//     {
//         id: 0,
//         img: 'https://images-na.ssl-images-amazon.com/images/I/412aXaVP%2BAL.jpg',
//         ques: 10,
//         title: 'sports'
//     },
//     {
//         id: 0,
//         img: 'https://images-na.ssl-images-amazon.com/images/I/412aXaVP%2BAL.jpg',
//         ques: 10,
//         title: 'sports'
//     }
// ]
class categoryForPVP extends Component {
    constructor(props) {
        super(props)
    }
    componentDidMount() {
        post('popularquiz')
            .then(res => {
                if (res.data.success) {
                    const { popularquiz } = res.data;
                    this.props.fetchPopularQuiz(popularquiz);
                    this.setState({ loading: false });
                }
            })
            .catch(err => {
                this.setState({ loading: false });
                throw new Error('Could not fetch products. Try again later.');
            });
    }

    render() {
        const { popularQuiz } = this.props;
        // console.log(popularQuiz)
        return (
            <React.Fragment>
                <TemplateWithGoBackButton {...this.props}/>
                <div style={{ margin: '15px', paddingTop: '40px' }}>
                    <div className="block-title">
                        <h3>PVP Categories</h3>
                    </div>
                    {popularQuiz.map((quiz, key) => (
                        <div key={key} className="thumb">
                            <Link to={{ pathname: `serching-pvp-player/${quiz.id}`, state: { quiz: quiz } }}
                                style={{ position: 'relative', display: 'block' }}>
                                <img
                                    className="img-responsive popular-quiz-img"
                                    src={BASE_URL_IMAGE + quiz.title_image}
                                    style={{
                                        width: '100%',
                                        height: '160px'
                                    }}
                                />
                                <div className="title" style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis', maxWidth: '100%' }}>{quiz.title}</div>
                                <div className="no-of-question">
                                    {/* {quiz.total} */}
                                    5 questions </div>
                            </Link>
                        </div>

                    ))}
                    <div className="clearfix"></div>
                </div>

            </React.Fragment>
        )
    }
}
const mapStateToProps = (state) => ({
    popularQuiz: state.quiz.popular,
});

const mapActionsToProps = {
    fetchPopularQuiz: fetchPopularQuiz,
};

export default connect(mapStateToProps, mapActionsToProps)(categoryForPVP);