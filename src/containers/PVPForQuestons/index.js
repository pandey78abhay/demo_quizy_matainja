import React, { Component } from 'react';
import Swal from 'sweetalert2';
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import closeIcon from '../../assets/close-icon.png';
import QuizPlayTemplate from '../quiz/components/QuizPlayTemplate';
import { post } from '../../http-api';
import PlayingPVPQuiz from './PlayingPVPQuiz';
import { fetchPVPQuestionByCatId } from '../../store/actions';
import { resetPVPPlayedQuiz, selectPVPQuestion, fetchPVPAnswerByCatId } from '../../store/actions';
import { DetailsSimmer } from '../../content-loader';
class PVPForQuestons extends Component {
    constructor(props) {
        super(props);
        this.state = {
           
            showQuiz: false

        }
    }
    componentDidMount() {
        


        this.props.resetPVPPlayedQuiz()
        this.props.selectPVPQuestion(1)
        // console.log(this.props.userId)
        
        const payload = new FormData();
        payload.append('category_id', this.props.categoryId)
        payload.append('pair', JSON.stringify(this.props.pair_ids))
        post('pvp_getquestion', payload)
            .then(res => {
                // console.log(res.data.player_1_questionDetails)
                if (res.data.player_1_questionDetails.user_id == this.props.userId) {
                    // console.log(res.data.player_1_questionDetails.question_list, 'reduxxx data sending player_1_questionDetails')

                    this.props.fetchPVPQuestionByCatId(res.data.player_1_questionDetails.question_list);
                    this.props.fetchPVPAnswerByCatId(res.data.player_1_questionDetails.answare_list);
                    this.setState({ showQuiz: true })
                }
                else if (res.data.player_2_questionDetails.user_id == this.props.userId) {
                    // console.log(res.data.player_2_questionDetails.question_list, 'reduxxx data sending player_2_questionDetails')
                    this.props.fetchPVPQuestionByCatId(res.data.player_2_questionDetails.question_list);
                    this.props.fetchPVPAnswerByCatId(res.data.player_2_questionDetails.answare_list);
                    this.setState({ showQuiz: true })
                }
                

            })
            .catch(err => console.log(err))
    }
    quitQuiz = () => {
        Swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.props.history.push('/category-pvp')
            }
        })
    }

    render() {
        // console.log(this.props.pair_id, 'pair_id')
        const { showQuiz } = this.state;
        return (
            <div>

                <div class="block-title"><h3>Quiz Play</h3></div>
                {!showQuiz ?
                    <QuizPlayTemplate>
                        <div>
                            <DetailsSimmer />
                            <DetailsSimmer />
                            <DetailsSimmer />
                        </div>
                    </QuizPlayTemplate>
                    :
                    <PlayingPVPQuiz pairId={this.props.pair_id} self_userId={this.props.userId} categoryId={this.props.categoryId} />
                }

                

            </div>
        )
    }
}

const mapStateToProps = (state) => ({});

const mapActionsToProps = {
    fetchPVPQuestionByCatId: fetchPVPQuestionByCatId,
    resetPVPPlayedQuiz: resetPVPPlayedQuiz,
    selectPVPQuestion: selectPVPQuestion,
    fetchPVPAnswerByCatId: fetchPVPAnswerByCatId
};

export default connect(mapStateToProps, mapActionsToProps)(withRouter(PVPForQuestons));