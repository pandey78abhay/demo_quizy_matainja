import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Swal from 'sweetalert2';
import { connect } from 'react-redux';
import { fetchPlayers } from '../../store/actions';
import { isLoggedIn, loggedInUserDetails } from '../../helper/authentication';
import { post } from '../../http-api';
import PVPImgage from '../../../assets/img/pvp.png';
import PairDetails from '../pairForDetails';
class matchedPVPPlayers extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // categoryId: this.props.history.location.state.categoryId,
            // userId: this.props.history.location.state.userId,
            // searching: true,
            searchScreen: true,
            pair_details: [],
            waitingTimeForPairCreate: true

        }
    }
    componentDidMount() {

        this.hit_pvp_noOfuserEnter();
        // console.log(this.state.waitingTimeForPairCreate)
        if (this.state.waitingTimeForPairCreate) {
            // console.log('trueee')
            this.turnOffApiHitTimeout = setTimeout(() => {
                this.setState({ waitingTimeForPairCreate: false })
                clearInterval(this.interval);
                Swal({
                    position: 'center',
                    type: 'error',
                    title: 'Your Pair Searching Time has been Expired,Try again!!!',
                    // showConfirmButton: true

                })
                    .then((result) => {
                        if (result.value) {
                            const userDetails = loggedInUserDetails()
                            const payload = new FormData();
                            payload.append("category_id", this.props.categoryId)
                            payload.append("user_id", userDetails.id_user)
                            post('pvp_SingleUserDeActivation', payload)
                                .then(res => {
                                    // console.log(res)
                                    if (res.data.success) {
                                        // console.log('deactivated user')

                                        this.props.history.push('/category-pvp')
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                })
                            // this.props.history.push('/category-pvp')
                        }
                    })

            }, 60000)
            if (this.state.waitingTimeForPairCreate) {
                this.interval = setInterval(this.hit_pvp_noOfuserEnter.bind(this), 1500);
            }



        }
    }
    
    componentWillUnmount(){
        console.log('componentWillUnmount');
        clearInterval(this.interval);
        clearInterval(this.turnOffApiHitTimeout);
    }
    hit_pvp_noOfuserEnter = () => {
        // console.log(this.props.history.location.state.userId)
        // console.log(userId)
        const userDetails = loggedInUserDetails()
        const payload = new FormData();
        payload.append("category_id", this.props.categoryId)
        payload.append("user_id", userDetails.id_user)
        // payload.append("user_status", 1)
        post('pvp_noOfuserEnter', payload)
            .then(res => {
                // console.log(res)
                // console.log(res.data.success)
                if (res.data.status == 200 && res.data.error == 0 && res.data.success == 1) {
                    clearInterval(this.turnOffApiHitTimeout);
                    console.log('pair found')
                    clearInterval(this.interval);

                    post('pvp_PlayerSearch', payload)
                        .then(res => {
                            console.log(res)
                            // this.setState({ searching: false })
                            if (res.data.status == 200 && res.data.error == 0 && res.data.success == 1 && res.data.show_notification == "No") {
                                Swal({
                                    position: 'center',
                                    type: 'success',
                                    title: 'Pairing request has been sent',
                                    showConfirmButton: true

                                })
                                    .then((result) => {
                                        if (result.value) {
                                            console.log(res.data)
                                            // this.setState({searchScreen:false})
                                            // this.props.history.push({
                                            //     pathname: '/pair-details',
                                            //     state: { pair_details: res.data, categoryId: categoryId, userId: userId }
                                            // })
                                            this.setState({
                                                pair_details: res.data,
                                                // categoryId: categoryId,
                                                // userId: userId
                                            })
                                            this.setState({ searchScreen: false })


                                        }
                                    })
                            }
                            else if (res.data.status == 200 && res.data.error == 0 && res.data.success == 1 && res.data.show_notification == "YES") {
                                Swal({
                                    position: 'center',
                                    type: 'success',
                                    title: 'Accept Pairing Request',
                                    showConfirmButton: true

                                })
                                    .then((result) => {
                                        if (result.value) {
                                            console.log(res.data)

                                            // this.props.history.push({
                                            //     pathname: '/pair-details',
                                            //     state: { pair_details: res.data, categoryId: categoryId, userId: userId }
                                            // })
                                            this.setState({
                                                pair_details: res.data,
                                                // categoryId: categoryId,
                                                // userId: userId
                                            })
                                            this.setState({ searchScreen: false })

                                        }
                                    })
                            }



                        })
                        .catch(err => console.log(err))

                }
                else if (res.data.status == 400 && res.data.error == 1 && res.data.success == 0 && res.data.pair == "No") {
                    // console.log('pair not found')
                    // console.log('data not found')

                }
                else if (res.data.status == 400 && res.data.error == 1 && res.data.success == 0 && res.data.pair == "Expired") {
                    // clearInterval(this.interval);
                    // console.log('pair not found')
                    // console.log('pair expired')
                    
                    const payload = new FormData();
                    payload.append('category_id', this.props.categoryId);
                    payload.append('pair_id', res.data.pair_id)
                    post('pvp_PairDeActivation', payload)
                        .then(res => {
                            console.log(res.data.message)
                        })
                    
                }
            })
            .catch(err => console.log(err))
    }

    render() {
        const userDetails = loggedInUserDetails()
        // console.log(this.state.pair_details, 'pair_details')
        // console.log(this.state.waitingTimeForPairCreate, 'change')
        return (

            <div>
                {this.state.searchScreen &&
                        <div>
                            <h2 style={{ textAlign: 'center', paddingTop: '20px' }}>Searching For Players</h2>
                            <div className="container" style={{ padding: '50px' }}>

                                <img src={PVPImgage} />
                                <h2 style={{ textAlign: 'center', paddingTop: '20px' }}>Waiting...</h2>
                                {/* <div className="container text-center">
                                    <button onClick={() => this.props.history.goBack()}
                                        class="pure-material-button-contained"
                                        style={{
                                            textTransform: 'capitalize', top: '15px', letterSpacing: '0.5px', color: '#fff', backgroundColor: 'black', height: '45px',
                                            width: '120px',
                                            fontSize: '22px',
                                            borderRadius: '15px 15px 15px 15px'
                                        }}><span></span>Cancel</button>
                                </div> */}
                            </div>
                        </div>



                    

                }
                {!this.state.searchScreen &&
                    <PairDetails categoryId={this.props.categoryId} userId={userDetails.id_user} pair_details={this.state.pair_details} />
                }

            </div>


        )
    }
}
const mapStateToProps = (State) => ({

})

const mapActionsToProps = {
    fetchPlayers: fetchPlayers
}

export default connect(mapStateToProps, mapActionsToProps)(withRouter(matchedPVPPlayers));