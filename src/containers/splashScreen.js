import React from 'react';
import Swiper from 'react-id-swiper';
import '../../assets/css/how-to-play.css';
import reward from '../../assets/img/reward.png';
import play from '../../assets/img/play.png';

//params of swiper
const params = {
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true,

    },
    height: '30px',
    backgroundColor: '#fff',
    slidesPerView: 1,
    spaceBetween: 10
};

//swiper data
const data = [

    {
        id: 0,
        image: play,
        smallText: "LET'S PLAY",
        longText: "Let see how smart are you. Go play QUIZY now! Ans all the following question and collect all the point. Redeem points with various of prize."

    },
    {
        id: 1,
        image: reward,
        smallText: "COLLECT AND REDEEM",
        longText: "Play all the quiz QUIZY, collect the points and redeem it all with various of prize"

    }
]
class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
        this.swiperRef = React.createRef();
        this.state = {
            visible: true,
        }

    }
    //splash screen off for this user. again will not show in future
    closeModal = () => {
        this.setState({
            visible: false
        });
        localStorage.removeItem('userDetailsSplash')

    }
    //next screen of splash
    nextButton = (id) => {
        console.log(this.swiperRef.current.swiper)
        if (this.swiperRef.current && this.swiperRef.current.swiper) {
            this.swiperRef.current.swiper.slideNext();
        }
    };
    render() {
        return (
            //splash screen design
            <div>
                {this.state.visible &&
                    <div id="modal-payment" className="modal" tabIndex="-1" role="dialog" aria-labelledby="mySmallModalLabel" style={{ display: 'block', position: 'fixed' }}>
                        <Swiper ref={this.swiperRef} {...params}>
                            {data.map(data => (
                                <div id="modal-htp" class="modal" tabindex="-1" role="dialog" style={{ display: 'block', paddingTop: '80px', backdropFilter: 'blur(2px)', backdropFilter: 'brightness(60%)' }}>
                                    <div className="modal-dialog modal-sm" role="document">
                                        <div className="modal-content">
                                            <div className="modal-body text-center">
                                                <div id="how-to-play-slider" class="carousel slide">

                                                    <div className="carousel-inner" role="listbox">

                                                        <div className="item active">
                                                            <div className="banner" style={{ left: 0, position: 'inherit', top: '-30px' }}><img src={data.image} alt="" /></div>
                                                            <div className="desc">
                                                                <h4><strong>{data.smallText}</strong></h4>
                                                                <p>{data.longText}</p>
                                                            </div>

                                                            <div className="control-slide" style={{ top: '80%' }}>
                                                                <div className="d-flex j-between">
                                                                    {data.id === 0 &&
                                                                        <button className="nextButton"
                                                                            onClick={() => this.nextButton(data.id)}
                                                                        ><strong>NEXT</strong></button>
                                                                    }

                                                                    <button className="skipButton" ><strong onClick={this.closeModal}>SKIP</strong></button>

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="d-flex h-100 ais-center modal-vertical-center">
                                        </div>
                                    </div>
                                </div>

                            ))}

                        </Swiper>

                    </div>
                }

            </div>
        )
    }
}
export default SplashScreen;