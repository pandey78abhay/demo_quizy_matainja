import React, { Component } from 'react';
import { post } from '../http-api';
import TemplateWithGoBackButton from '../template/template-with-goback-button';
import './privacy-PrivacyPolicy.css';

export default class PrivacyPolicy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content: null
        };
    }
    componentDidMount() {
        const payload = new FormData();
        payload.append("page", "Privacypolicy");
        post('getpage', payload)
            .then(res => {
                this.setState({
                    content: res.data.page_details[0].content
                });
            });
    }
    render() {
        const { content } = this.state;
        return(
            <TemplateWithGoBackButton {...this.props}>
		        <div className="block-title block-position-fixed">
			        <h3>Privacy and Policy</h3>
		        </div>
                <div className="pd-15 mt-20"></div>
                <div className="block pd-15" style={{overflowY: 'auto'}} dangerouslySetInnerHTML={{ __html: content }}/>
            </TemplateWithGoBackButton>
        );
    }
};

