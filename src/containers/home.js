import React, { Component } from 'react';
import { connect } from "react-redux";
import SwiperSlider from '../components/slider/swiper-slide';
import PopularQuiz from './home/components/popular-quiz';
import PVPButton from '../components/PVP/PVPButton';
import LatestQuiz from '../components/quiz/latest';
import TemplateWithSlideBar from '../template/template-with-slide-bar';
import { fetchBannerDetails } from '../store/actions';
import { post } from '../http-api';
import SplashScreen from './splashScreen';
import { isLoggedIn, loggedInUserDetailsSplash, loggedInUserDetails } from '../../src/helper/authentication';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            userLoginCount: 0,
            firstVisit: false
        }
    }
    componentDidMount() {
        post('getbannerlist')
            .then(res => {
                if (res.data.success) {
                    const { banner_details } = res.data;
                    this.props.fetchBannerDetails(banner_details);
                }
            })
            .catch(err => {
                throw new Error('Could not fetch products. Try again later.');
            });

        // if user logged in count value store in state
        if (loggedInUserDetailsSplash()) {
            const userLoginCountTemp = loggedInUserDetailsSplash().login_count;
            this.setState({ userLoginCount: userLoginCountTemp })
        }
        //when user not login but need to show splash screen
        if (loggedInUserDetails() === null) {
            console.log('not login')
            const firstVisit = localStorage.getItem('userFirstVisit', true);
            console.log(firstVisit);
            //first time to this site for a user in a perticular browser
            if (firstVisit === null) {
                localStorage.setItem('userFirstVisit', true);
                console.log('first visit')
                this.setState({ firstVisit: true })
            }
            //not first time to this site for a user in a perticular browser
            else if (firstVisit !== null) {

                console.log('not first time visit')
            }


        }
    }


    render() {
        const isLogin = loggedInUserDetailsSplash();

        return (
            <TemplateWithSlideBar>
                <SwiperSlider />
                <div className="block-title">
                    <h3>Popular Quiz</h3>
                </div>

                {isLogin && this.state.userLoginCount == 0 &&
                    //splash screen for first time login user
                    <SplashScreen firstVisit={this.state.firstVisit} />
                }
                {this.state.firstVisit == true &&
                    //splash screen for not loggedin user & first time from particular browser
                    <SplashScreen />
                }

                <PopularQuiz />
                {/* <div className="block-title">
                    <h3>1 VS 1 (PVP)</h3>
                </div>
                <div className="block-title" style={{
                    background: 'none', padding: '15px', justifyContent: 'center'
                }}>
                    <PVPButton />
                </div> */}
                <div className="block-title">
                    <h3>Latest Quiz</h3>
                </div>
                <LatestQuiz />
            </TemplateWithSlideBar>
        );
    }
};


const mapStateToProps = (state) => ({
    popularQuiz: state.popularQuiz,
});

const mapActionsToProps = {
    fetchBannerDetails: fetchBannerDetails
};

export default connect(mapStateToProps, mapActionsToProps)(Home);