
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import Swal from 'sweetalert2';
import { post_loggedInUser } from '../../http-api';
import { BASE_URL_IMAGE } from '../../http-api';
import { post } from '../../http-api';
import { fetchCategoryDetails } from '../../store/actions';
import { DetailsSimmer, TitleSimmer, DetailsBannerSimmer } from '../../content-loader';
import { isLoggedIn, loggedInUserDetails } from '../../helper/authentication';
import TemplateWithGoBackButton from '../../template/template-with-goback-button';
import PVPImgage from '../../../assets/img/pvp.png';
import MatchedForPVPPlayers from '../matchedForPVPPlayers';
class SearchForPVPPlayer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            noData: true,
            loading: false,
            categoryId: this.props.history.location.state.quiz.id,
            error: false,
            playScreen: true,
            searchScreen: false,
        }
    }
    componentDidMount() {
        const { categoryId } = this.state;
        console.log(isLoggedIn())
        if (isLoggedIn()) {
            // console.log('logged in')
            const userDetails = loggedInUserDetails()
            // console.log(userDetails.id_user)
            this.setState({ loading: true })
            var payload = new FormData();
            payload.append("category_id", categoryId);
            post_loggedInUser('pvpquizcategory', payload)
                .then(res => {
                    // console.log(res)
                    if (res.data.success == 1) {
                        // console.log(res.data.category_details)
                        const categoryDetails = res.data.category_details;
                        // console.log(categoryDetails)
                        this.props.fetchCategoryDetails(categoryDetails);
                        this.setState({ loading: false })
                        this.setState({ noData: false })
                    }
                    else if (res.data.success == 0 && res.data.error == 1) {
                        // console.log('no data')
                        this.setState({ noData: true })
                        this.setState({ loading: false })
                        this.setState({ error: true })
                    }
                })
                .catch(err => { console.log(err) })
        }
        else {
            // console.log('no logged in')
            Swal({
                position: 'center',
                type: 'warning',
                title: 'You have to Login first',
                showConfirmButton: true

            }).then((result) => {
                if (result.value) {
                    this.props.history.push('/login')
                }
            })
        }

    }
    componentWillMount() {
        clearInterval(this.interval);
    }
    categoryPVPpage = () => {
        this.props.history.push({
            pathname: '/category-pvp'
        });
    }
    searchPlayer = () => {
        // console.log('searching');

        this.setState({ playScreen: false, searchScreen: true })

    }



    render() {
        const { categoryDetails } = this.props;
        const { noData, loading, error } = this.state;
        // console.log(categoryDetails)

        return (

            <React.Fragment>
                {/* {console.log(noData, loading)} */}

                <TemplateWithGoBackButton>
                    {this.state.playScreen && !this.state.searchScreen &&
                        <div>
                            {noData && !loading && error &&
                                <div>NO DATA FOUND</div>
                            }
                            {loading &&
                                <div>
                                    <DetailsSimmer />
                                    <DetailsSimmer />
                                </div>
                            }
                            {!noData && !loading &&
                                <div className="col-xs-12 pd-10">
                                    <div className="rewards-card">
                                        <div className="fixed-off">
                                            <div className="fixed-off1">
                                                <div className="rewards-img">
                                                    {categoryDetails.title_image && <img
                                                        height="150px"
                                                        src={BASE_URL_IMAGE + categoryDetails.title_image}
                                                        style={{ width: '100%', objectFit: 'cover', objectPosition: 'top center' }}
                                                        className="image-responsive"
                                                    />}
                                                    <div className="coin"><strong>

                                                        5 Questions
                                                 </strong></div>

                                                </div>
                                                <div className="pd-10">
                                                    <div className="col-xs-9 pd-0">
                                                        <h4 className="mt-0" style={{ textTransform: 'capitalize' }}>{categoryDetails.title}</h4>

                                                        <div className="info-game-rating">
                                                            <span className="info-game-rating">
                                                                <i className="fa fa-star"></i>
                                                                <i className="fa fa-star"></i>
                                                                <i className="fa fa-star"></i>
                                                                <i className="fa fa-star"></i>
                                                                <i className="fa fa-star"></i>
                                                            </span>
                                                        </div>
                                                        <div className="mt-0" style={{ textTransform: 'capitalize' }}>{categoryDetails.description}</div>
                                                    </div>
                                                    <div className="col-xs-3 pd-0">
                                                        {Object.keys(categoryDetails).length &&
                                                            <button onClick={this.searchPlayer}
                                                                className="pure-material-button-contained"
                                                                style={{ textTransform: 'capitalize', top: '15px', letterSpacing: '0.5px', color: '#fff', backgroundColor: 'rgb(50, 205, 50)', }}
                                                            ><span style={{ paddingRight: '5px' }}>&#9658;</span>Play</button>}
                                                    </div>
                                                    <div className="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            }
                        </div>
                    }
                    {this.state.searchScreen && !this.state.playScreen &&
                        <MatchedForPVPPlayers categoryId={this.state.categoryId} />


                    }
                </TemplateWithGoBackButton>


            </React.Fragment>
        )
    }
}
const mapStateToProps = (state) => ({
    categoryDetails: state.categoryDetails,
});

const mapActionsToProps = {
    fetchCategoryDetails: fetchCategoryDetails,
};
export default connect(mapStateToProps, mapActionsToProps)(withRouter(SearchForPVPPlayer));