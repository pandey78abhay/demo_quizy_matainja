import React from 'react';
import ProfileDetails from '../components/profile/details';
import QuizHistory from '../components/quiz/history';
import TemplateWithSlideBar from '../template/template-with-slide-bar';
const Profile = () => (
    <TemplateWithSlideBar>
        <ProfileDetails />
        <QuizHistory />
    </TemplateWithSlideBar>
);

export default Profile;